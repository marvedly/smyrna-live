swedish: Smyrnakyrkan, Predikan, Smyrna, Talare, Kristen, Kristendom, Gud, Jesus
english: Smyrna Church, Sermon, Smyrna, Speaker, Christian, Christianity, God, Jesus
farsi: Smyrna Church, Sermon, Smyrna, Speaker, Christian, Christianity, God, Jesus
other: Smyrnakyrkan, Smyrna, Kristen, Kristendom, Gud, Jesus

Tags should be on the format above separated by commas.

ENCODING: ANSI