package se.smyrna.smyrnalive.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.NoSuchElementException;

import se.smyrna.smyrnalive.model.events.IEvent;
import se.smyrna.smyrnalive.utils.FileUtils;

/**
 * Settings for the program
 * @author Martin
 *
 */
public class ProgramSettings {
	private static ProgramSettings programSettings;

	/**
	 * Folder path to know where to put XML-files and so on...
	 */
	public final String DEFAULT_PROGRAM_RESOURCE_LOCATION = System.getProperty("user.home") + "\\Smyrna Live Resources";
	public final String DEFAULT_XML_FILE_PATH = DEFAULT_PROGRAM_RESOURCE_LOCATION + "\\YouTube Live XML";
	public final String DEFAULT_MXLIGHT_PATH = DEFAULT_PROGRAM_RESOURCE_LOCATION + "\\mxlight";
	public final String DEFAULT_CONFIG_FOLDER_PATH = DEFAULT_PROGRAM_RESOURCE_LOCATION + "\\config";
	public final String DEFAULT_DESCRIPTION_FOLDER_PATH = DEFAULT_CONFIG_FOLDER_PATH + "\\descriptions";
	public final String DEFAULT_MXLIGHT_RECORDING_FOLDER_NAME = "\\recordings";
	public final String DEFAULT_DO_NOT_TOUCH_FILE_LOCATION = DEFAULT_PROGRAM_RESOURCE_LOCATION + "\\doNotTouch.smy";
	private final int DEFAULT_HOURS = 2;
	private final int DEFAULT_MINS = 0;
	private String xmlFolderLocation;

	private String programResourceLocation;
	private String mxlightLocation;
	private String mxlightRecordingFolderPath;

	private TimeInstance defaultStreamingLength;
	private File settingsFile;
	private File preScheduledFile;
	private File tagsFile;
	private String tags;
	private int startHour = -1;
	private int startMin = -1;
	private String serviceLanguage = "other"; // Default event (overridden by preScheduled though)
	private File preachersFile;
	private ArrayList<String> preachers;
	private File seriesFile;
	private ArrayList<String> series;
	private ArrayList<String> seriesNbr;
	private String lastReadDescription = ""; // default value

	/**
	 * Constructor that reads the saved folder path
	 */
	private ProgramSettings() {
		readSettings();
		readPreScheduled();
		readPreachers();
		readSeries();
		createImageFolderAndImageReadme();
	}

	private void createImageFolderAndImageReadme() {
		createFolderIfNotExist(programResourceLocation + "\\images");
		BufferedWriter writer = null;
		try {
			File imageReadmeFile = new File(programResourceLocation + "\\images\\Images readme.txt");
			if (!imageReadmeFile.exists()) {

				imageReadmeFile.createNewFile();

				writer = new BufferedWriter(new FileWriter(imageReadmeFile));
				writer.write("Replace the picture that you want to upload as a thumbnail on YouTube.");
				writer.newLine();
				writer.newLine();
				writer.write("The names are...");
				writer.newLine();
				writer.newLine();
				writer.write("thumbnail swedish.png");
				writer.newLine();
				writer.write("thumbnail english.png");
				writer.newLine();
				writer.write("thumbnail farsi.png");
				writer.newLine();
				writer.write("thumbnail other.png");
				writer.newLine();
				writer.newLine();
				writer.write("... for the different kinds of events");
				writer.newLine();
				writer.newLine();
				writer.write("********************************************************");
				writer.newLine();
				writer.write("** The images must be put inside the \"images\"-folder. **");
				writer.newLine();
				writer.write("********************************************************");
			}
		} catch (IOException e) {
			// Could not save file!
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {}
			}
		}

	}

	public static synchronized ProgramSettings getInstance() {
		if (programSettings == null) {
			programSettings = new ProgramSettings();
		}
		return programSettings;
	}

	public void createFolderIfNotExist(String path) {
		File folder = new File(path);
		if (!folder.exists()) {
			folder.mkdirs();
		}
	}

	private void readSettings() {
		boolean readingOk = false;
		settingsFile = new File(DEFAULT_CONFIG_FOLDER_PATH + "\\settings.smy");

		String programResourcePath = null;
		String mxlightPath = null;
		String xmlFolderPath = null;
		int hoursInt = 0;
		int minInt = 0;

		if (settingsFile.exists()) {
			readingOk = true;
			BufferedReader reader = null;

			try {
				reader = new BufferedReader(new FileReader(settingsFile));

				// read programResourcePath
				try {
					programResourcePath = reader.readLine();
					if (programResourcePath == null || !FileUtils.isFilenameValid(programResourcePath, "")) {
						//corrupt file
						readingOk = false;
					} else {
						programResourcePath = programResourcePath.replace("\"", "");
					}
				} catch (IOException e) {
					e.printStackTrace();
					readingOk = false;
				}

				// read mxlightPath
				try {
					mxlightPath = reader.readLine();
					if (mxlightPath == null || !FileUtils.isFilenameValid(mxlightPath, "")) {
						//corrupt file
						readingOk = false;
					} else {
						mxlightPath = mxlightPath.replace("\"", "");
					}
				} catch (IOException e) {
					e.printStackTrace();
					readingOk = false;
				}

				// read xmlFolderLocation
				try {
					xmlFolderPath = reader.readLine();
					if (xmlFolderPath == null) {
						// corrupt file
						readingOk = false;
					} else {
						xmlFolderPath = xmlFolderPath.replace("\"", "");
						if (!FileUtils.isFilenameValid(xmlFolderPath, "")) {
							//corrupt file
							readingOk = false;
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
					readingOk = false;
				}

				// read hours
				String hours = null;
				try {
					hours = reader.readLine();
					if (hours == null) {
						//corrupt file
						readingOk = false;
					}
					hoursInt = Integer.parseInt(hours);

				} catch (IOException e) {
					e.printStackTrace();
					readingOk = false;
				} catch (NumberFormatException e) {
					//not an integer
					readingOk = false;
				}

				//read minutes
				String min = null;
				try {
					min = reader.readLine();
					if (min == null) {
						//corrupt file
						readingOk = false;
					}
					minInt = Integer.parseInt(min);
				} catch (IOException e) {
					e.printStackTrace();
					readingOk = false;
				} catch (NumberFormatException e) {
					//not an integer
					readingOk = false;
				}		

			} catch (FileNotFoundException e) {
				e.printStackTrace();
				readingOk = false;
			} finally {
				if (reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
						settingsFile.delete();
					}
				}
			}
		}
		if (readingOk) {
			programResourceLocation = programResourcePath; 
			mxlightLocation = mxlightPath;
			xmlFolderLocation = xmlFolderPath;
			defaultStreamingLength = new TimeInstance(hoursInt, minInt);
		} else {
			programResourceLocation = DEFAULT_PROGRAM_RESOURCE_LOCATION;
			mxlightLocation = DEFAULT_MXLIGHT_PATH;
			xmlFolderLocation = DEFAULT_XML_FILE_PATH;
			defaultStreamingLength = new TimeInstance(DEFAULT_HOURS, DEFAULT_MINS);
		}
		mxlightRecordingFolderPath = mxlightLocation + DEFAULT_MXLIGHT_RECORDING_FOLDER_NAME;
		saveSettings();
	}

	private void readPreScheduled() {
		preScheduledFile = new File(DEFAULT_CONFIG_FOLDER_PATH + "\\preScheduled.smy");
		if (!preScheduledFile.exists()) {
			newPreScheduled();
		}
		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new FileReader(preScheduledFile));
			String info = reader.readLine().toLowerCase();
			while (info != null) {
				info = info.trim();
				if (info.matches("\\d \\d\\d? \\d\\d? (swedish|english|farsi|other)")) {
					String[] parameters = info.split(" ");
					if (Calendar.getInstance().get(Calendar.DAY_OF_WEEK) == Integer.parseInt(parameters[0])) {
						int tmpStartHour = Integer.parseInt(parameters[1]);
						int tmpStartMin = Integer.parseInt(parameters[2]);
						String tmpServiceLanguage = parameters[3];
						if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) < tmpStartHour
								|| (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) == tmpStartHour &&
								Calendar.getInstance().get(Calendar.MINUTE) < tmpStartMin)) {
							startHour = tmpStartHour;
							startMin = tmpStartMin;
							serviceLanguage = tmpServiceLanguage;
							break;
						}
					}
				}
				info = reader.readLine();
			}

		} catch (FileNotFoundException e) {
			// won't happen
		} catch (NoSuchElementException e) {
			preScheduledFile.delete();
			newPreScheduled();
			e.printStackTrace();
		} catch (IOException e) {
			// something wrong with the file
			preScheduledFile.delete();
			newPreScheduled();
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					preScheduledFile.delete();
					newPreScheduled();
				}
			}
		}
	}

	private void newPreScheduled() {
		createFolderIfNotExist(DEFAULT_CONFIG_FOLDER_PATH);
		// file does not exist! Create a new default file
		BufferedWriter writer = null;
		try {
			preScheduledFile.createNewFile();

			writer = new BufferedWriter(new FileWriter(preScheduledFile));
			writer.write("1 11 0 swedish"); // Sunday 11 am
			writer.newLine();
			writer.write("1 15 0 english"); // Sunday 15 am
			writer.newLine();
			writer.write("1 19 0 swedish"); // Sunday 19 am
			writer.newLine();
			writer.write("5 18 30 farsi"); // Thursday 18:30 am
		} catch (IOException e) {
			// Could not save file!
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {}
			}
		}
	}

	/**
	 * 
	 * @return true if settings were saved to file, else false
	 */
	public boolean saveSettings() {
		createFolderIfNotExist(DEFAULT_CONFIG_FOLDER_PATH);
		boolean savingOk = true;
		BufferedWriter writer = null;
		try {
			if (settingsFile.exists()) {
				settingsFile.delete();
			}
			settingsFile.createNewFile();

			writer = new BufferedWriter(new FileWriter(settingsFile));
			writer.write(programResourceLocation);
			writer.newLine();
			writer.write(mxlightLocation);
			writer.newLine();
			writer.write(xmlFolderLocation);
			writer.newLine();
			writer.write(String.valueOf(defaultStreamingLength.getHour()));
			writer.newLine();
			writer.write(String.valueOf(defaultStreamingLength.getMinute()));
		} catch (IOException e) {
			savingOk = false;
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {}
			}
		}
		return savingOk;
	}

	public void readTags(IEvent[] events) {
		tagsFile = new File(DEFAULT_CONFIG_FOLDER_PATH + "\\tags.smy");
		if (!tagsFile.exists()) {
			newTagsFile();
		}
		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new FileReader(tagsFile));
			String info = reader.readLine();
			while (info != null) {
				info = info.trim();
				if (info.matches("(swedish|english|farsi|other):.*")) {
					IEvent event = null;
					if (info.startsWith("swedish")) {
						event = events[0];
					} else if (info.startsWith("english")) {
						event = events[1];
					} else if (info.startsWith("farsi")) {
						event = events[2];
					} else {
						event = events[3];
					}
					info = info.substring(info.indexOf(":") + 1).trim();
					String[] parameters = info.split(",");
					StringBuilder sb = new StringBuilder();
					for (int i = 0; i < parameters.length; i++) {
						if (!parameters[i].isEmpty() && !sb.toString().contains(parameters[i])) {
							sb.append(parameters[i]);
							if (i != parameters.length-1) {
								sb.append(",");
							}
						}
					}
					event.setTags(sb.toString());
				}
				info = reader.readLine();
			}

		} catch (FileNotFoundException e) {
			// won't happen
		} catch (NoSuchElementException e) {
			tagsFile.delete();
			newTagsFile();
			e.printStackTrace();
		} catch (IOException e) {
			// something wrong with the file
			tagsFile.delete();
			newTagsFile();
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					tagsFile.delete();
					newTagsFile();
				}
			}
		}
	}

	private void newTagsFile() {
		// file does not exist! Create a new default file
		createFolderIfNotExist(DEFAULT_CONFIG_FOLDER_PATH);
		BufferedWriter writer = null;
		try {
			tagsFile.createNewFile();

			writer = new BufferedWriter(new FileWriter(tagsFile));
			writer.newLine();
			writer.write("swedish: Smyrnakyrkan, Predikan, Smyrna, Talare, Kristen, Kristendom, Gud, Jesus");
			writer.newLine();
			writer.write("english: Smyrna church, Sermon, Smyrna, Speaker, Christian, Christianity, God, Jesus");
			writer.newLine();
			writer.write("farsi: Smyrna church, Sermon, Smyrna, Speaker, Christian, Christianity, God, Jesus");
			writer.newLine();
			writer.write("other: Smyrnakyrkan, Smyrna, Kristen, Kristendom, Gud, Jesus");
		} catch (IOException e) {
			// Could not save file!
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {}
			}
		}
	}

	private void readPreachers() {
		preachersFile = new File(DEFAULT_CONFIG_FOLDER_PATH + "\\preachers.smy");
		if (!preachersFile.exists()) {
			newPreachers();
		}
		BufferedReader reader = null;
		preachers = new ArrayList<String>();
		try {
			reader = new BufferedReader(new FileReader(preachersFile));
			String preacher = reader.readLine();
			while (preacher != null) {
				if (preacher.length() > 0) {
					preachers.add(preacher);
				}
				preacher = reader.readLine();
			}

		} catch (FileNotFoundException e) {
			// won't happen
		} catch (NoSuchElementException e) {
			preachersFile.delete();
			newPreachers();
			e.printStackTrace();
		} catch (IOException e) {
			// something wrong with the file
			preachersFile.delete();
			newPreachers();
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					preachersFile.delete();
					newPreachers();
				}
			}
		}
	}

	private void newPreachers() {
		createFolderIfNotExist(DEFAULT_CONFIG_FOLDER_PATH);
		// file does not exist! Create a new default file
		BufferedWriter writer = null;
		try {
			preachersFile.createNewFile();

			writer = new BufferedWriter(new FileWriter(preachersFile));
			writer.write("Urban Ringb�ck");
			writer.newLine();
			writer.write("Tomas Sj�din");
			writer.newLine();
			writer.write("Johannes Magnusson");
			writer.newLine();
			writer.write("Anne-Jorid Ahgnell");
			writer.newLine();
			writer.write("Peter Lewin");
			writer.newLine();
			writer.write("Cornelia Forsberg");
			writer.newLine();
			writer.write("Mark Beckenham");
			writer.newLine();
			writer.write("Stefan Rizell");
			writer.newLine();
			writer.write("Belton Mubiru");
			writer.newLine();
			writer.write("Amadeus Ringb�ck");
			writer.newLine();
			writer.write("Lars Svensson");
		} catch (IOException e) {
			// Could not save file!
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {}
			}
		}
	}

	// eventType could be swedish, english, farsi or other
	public String readDescription(String eventType) {
		File descriptionFile = new File(DEFAULT_DESCRIPTION_FOLDER_PATH + "\\" + eventType + ".smy");
		if (!descriptionFile.exists()) {
			newDescription(descriptionFile);
		}
		BufferedReader reader = null;
		StringBuilder sb = new StringBuilder();
		try {
			reader = new BufferedReader(new FileReader(descriptionFile));
			String descriptionLine = reader.readLine();
			if (descriptionLine != null) {
				sb.append(descriptionLine);
				descriptionLine = reader.readLine();
				while (descriptionLine != null) {
					sb.append("\n");
					sb.append(descriptionLine);
					descriptionLine = reader.readLine();
				}
			}
			lastReadDescription = sb.toString();
		} catch (FileNotFoundException e) {
			// won't happen
		} catch (NoSuchElementException e) {
			descriptionFile.delete();
			newDescription(descriptionFile);
			e.printStackTrace();
		} catch (IOException e) {
			// something wrong with the file
			descriptionFile.delete();
			newDescription(descriptionFile);
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					descriptionFile.delete();
					newDescription(descriptionFile);
				}
			}
		}
		return lastReadDescription;
	}

	private void newDescription(File descriptionFile) {
		createFolderIfNotExist(DEFAULT_DESCRIPTION_FOLDER_PATH);
		// file does not exist! Create a new default file
		BufferedWriter writer = null;
		try {
			createFolderIfNotExist(DEFAULT_DESCRIPTION_FOLDER_PATH);
			descriptionFile.createNewFile();

		} catch (IOException e) {
			// Could not save file!
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {}
			}
		}
	}

	private void readSeries() {
		seriesFile = new File(DEFAULT_CONFIG_FOLDER_PATH + "\\series.smy");
		if (!seriesFile.exists()) {
			newSeries();
		}
		BufferedReader reader = null;
		series = new ArrayList<String>();
		seriesNbr = new ArrayList<String>();
		series.add("");
		seriesNbr.add("");
		try {
			reader = new BufferedReader(new FileReader(seriesFile));
			String info = reader.readLine();
			while (info != null) {
				if (info.length() > 0) {
					int endIndex = info.lastIndexOf("[");
					if (endIndex == -1) { // no number of parts specified
						justAddTheSeries(info);
					} else {
						String seriesName = info.substring(0, info.lastIndexOf("["));
						int ei =info.lastIndexOf("]");
						if (ei != -1) {
							String number = info.substring(info.lastIndexOf("[")+1, ei);
							if (number.matches("\\d+")) {
								series.add(seriesName.trim());
								seriesNbr.add(number);
							} else {
								justAddTheSeries(info);
							}
						} else {
							justAddTheSeries(info);
						}
					}
				}
				info = reader.readLine();
			}

		} catch (FileNotFoundException e) {
			// won't happen
		} catch (NoSuchElementException e) {
			seriesFile.delete();
			newSeries();
			e.printStackTrace();
		} catch (IOException e) {
			// something wrong with the file
			seriesFile.delete();
			newSeries();
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					seriesFile.delete();
					newSeries();
				}
			}
		}
	}

	private void justAddTheSeries(String info) {
		series.add(info.trim());
		seriesNbr.add(null);		
	}

	private void newSeries() {
		createFolderIfNotExist(DEFAULT_CONFIG_FOLDER_PATH);
		// file does not exist! Create a new default file
		BufferedWriter writer = null;
		try {
			seriesFile.createNewFile();

			writer = new BufferedWriter(new FileWriter(seriesFile));
			writer.write("");
		} catch (IOException e) {
			// Could not save file!
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {}
			}
		}
	}

	public String getProgramResourceLocation() {
		return programResourceLocation;
	}

	public String getMxlightLocation() {
		return mxlightLocation;
	}

	public String getxmlFolderLocation() {
		return xmlFolderLocation;
	}

	public void setxmlFolderLocation(String xmlFolderLocation) {
		this.xmlFolderLocation = xmlFolderLocation;
	}

	public TimeInstance getDefaultStreamingLength() {
		return defaultStreamingLength;
	}

	public void setDefaultStreamingLength(TimeInstance defaultStreamingLength) {
		this.defaultStreamingLength = defaultStreamingLength;
	}

	public int getStartHour() {
		return startHour;
	}

	public int getStartMin() {
		return startMin;
	}

	public String getServiceLanguage() {
		return serviceLanguage;
	}
	
	public void setServiceLanguage(String serviceLanguage) {
		this.serviceLanguage = serviceLanguage;
	}

	public String getTags(IEvent swedishService, IEvent internationalService, IEvent farsiService, IEvent otherEvent) {
		return tags;
	}

	public String getThumbnailPath() {
		return UserInput.getInstance().getType().getThumbnailLocation();
	}

	public String[] getPreachers() {
		String[] toReturn = new String[preachers.size()];
		return preachers.toArray(toReturn);
	}

	public String getLastReadDescription() {
		return lastReadDescription;
	}

	public String[] getSeries() {
		String[] toReturn = new String[series.size()];
		return series.toArray(toReturn);
	}

	public String[] getSeriesNbr() {
		String[] toReturn = new String[seriesNbr.size()];
		return seriesNbr.toArray(toReturn);
	}

	public String getStreamsCDAPath() {
		return mxlightLocation + "\\cfg\\strForStream.cda";
	}

	public String getMxlightRecordingFolderPath() {
		return mxlightRecordingFolderPath;
	}

	public void createMxlightSpecialFolders() {
		createFolderIfNotExist(mxlightLocation + "\\status");
		createFolderIfNotExist(mxlightLocation + "\\cfg");
		createFolderIfNotExist(mxlightRecordingFolderPath);
	}

	

}
