package se.smyrna.smyrnalive.model.events;

import se.smyrna.smyrnalive.model.ProgramSettings;

public class OtherEvent implements IEvent {
	private String tags;
	
	/**
	 * Not implemented.
	 */
	@Override
	public String getName() {
		return null;
	}

	@Override
	public String getTags() {
		return tags;
	}

	@Override
	public void setTags(String tags) {
		this.tags = tags;
	}

	@Override
	public String getThumbnailLocation() {
		return ProgramSettings.getInstance().getProgramResourceLocation() + "\\images\\thumbnail other.png";
	}

}
