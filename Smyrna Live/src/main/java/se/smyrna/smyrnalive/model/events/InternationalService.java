package se.smyrna.smyrnalive.model.events;

import se.smyrna.smyrnalive.model.ProgramSettings;

public class InternationalService implements DescriptableIEvent {
	private String tags;
	
	@Override
	public String getName() {
		return "International service";
	}

	@Override
	public String getTags() {
		return tags;
	}

	@Override
	public void setTags(String tags) {
		this.tags = tags;
	}

	@Override
	public String getThumbnailLocation() {
		return ProgramSettings.getInstance().getProgramResourceLocation() + "\\images\\thumbnail english.png";
	}

	@Override
	public String getDescription_sermon() {
		return "Sermon";
	}

	@Override
	public String getDescription_nbr() {
		return "Nbr.";
	}

	@Override
	public String getDescription_of() {
		return "of";
	}

	@Override
	public String getDescription_in_series() {
		return "in series";
	}

	@Override
	public String getDescription_Smyrna() {
		return "Smyrna church of Gothenburg";
	}

}
