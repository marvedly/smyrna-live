package se.smyrna.smyrnalive.model.events;

public interface DescriptableIEvent extends IEvent {
	String getDescription_sermon();
	String getDescription_nbr();
	String getDescription_of();
	String getDescription_in_series();
	String getDescription_Smyrna();
}
