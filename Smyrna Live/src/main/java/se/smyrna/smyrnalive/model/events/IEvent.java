package se.smyrna.smyrnalive.model.events;

public interface IEvent {
	String getName();
	String getTags();
	void setTags(String string);
	String getThumbnailLocation();
}
