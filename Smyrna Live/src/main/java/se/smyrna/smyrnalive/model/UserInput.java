package se.smyrna.smyrnalive.model;

import java.util.ArrayList;
import java.util.List;

import se.smyrna.smyrnalive.model.events.IEvent;

public class UserInput {
	private static UserInput userInput = null;
	private String broadCastTitle;
	public final String streamTitle = "Streaming Smyrna";
	private String startTime;
	private String endTime;
	private String description;
	private String privacyStatus;
	private boolean isOverwriting;
	private List<String> tags;
	private String location;
	private IEvent type;

	private UserInput() {}
	
	public synchronized static UserInput getInstance() {
		if (userInput == null) {
			userInput = new UserInput();
		}
		return userInput;
	}
	
	public String getBroadCastTitle() {
		return broadCastTitle;
	}

	public void setBroadCastTitle(String broadCastTitle) {
		this.broadCastTitle = broadCastTitle;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPrivacyStatus() {
		return privacyStatus;
	}

	public void setPrivacyStatus(String privacyStatus) {
		this.privacyStatus = privacyStatus;
	}

	public boolean isOverwriting() {
		return isOverwriting;
	}
	
	public void setOverwriting(boolean isOverwriting) {
		this.isOverwriting = isOverwriting;
	}

	public List<String> getTags() {
		return tags;
	}
	
	public void setTags(String[] tags) {
		this.tags = new ArrayList<String>(tags.length);
		for (String tag : tags) {
			this.tags.add(tag);
		}
	}
	
	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	public IEvent getType() {
		return type;
	}

	public void setType(IEvent type) {
		this.type = type;
	}
}
