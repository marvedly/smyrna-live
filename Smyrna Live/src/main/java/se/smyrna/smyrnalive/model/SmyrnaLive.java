package se.smyrna.smyrnalive.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import se.smyrna.smyrnalive.model.events.IEvent;

public class SmyrnaLive {
	private PropertyChangeSupport pcs;
	private String title;
	private String otherTitle;
	private String otherDescription;
	private String titleAsItWasWhenStreamStarted;
	private IEvent event;
	private boolean isRecording;
	private boolean isStreaming;
	private boolean isTitleChanged;
	private boolean isShowingLoadingIcon;
	private boolean isEventCreated;
	
	public SmyrnaLive() {
		pcs = new PropertyChangeSupport(this);
		title = "";
		otherTitle = "";
		otherDescription = "";
		event = null;
		isEventCreated = false;
	}
	
	public void addPropertyChangeListener(PropertyChangeListener pcl) {
		pcs.addPropertyChangeListener(pcl);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener pcl) {
		pcs.removePropertyChangeListener(pcl);
	}
	
	public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
		pcs.firePropertyChange(propertyName, oldValue, newValue);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
		firePropertyChange("title", null, title);
	}

	public String getOtherTitle() {
		return otherTitle;
	}

	public void setOtherTitle(String otherTitle) {
		this.otherTitle = otherTitle;
	}
	
	public String getOtherDescription() {
		return otherDescription;
	}

	public void setOtherDescription(String otherDescription) {
		this.otherDescription = otherDescription;
	}

	public IEvent getEvent() {
		return event;
	}

	public void setEvent(IEvent event) {
		if (event != null) {
			this.event = event;
		} else {
			this.event = null;
		}
	}

	public String getTitleAsItWasWhenStreamStarted() {
		return titleAsItWasWhenStreamStarted;
	}

	public void setTitleAsItWasWhenStreamStarted(
			String titleAsItWasWhenStreamStarted) {
		this.titleAsItWasWhenStreamStarted = titleAsItWasWhenStreamStarted;
	}

	public boolean isRecording() {
		return isRecording;
	}

	public void setRecording(boolean isRecording) {
		firePropertyChange("recording", this.isRecording, Boolean.valueOf(isRecording));
		this.isRecording = isRecording;
	}

	public boolean isStreaming() {
		return isStreaming;
	}

	public void setStreaming(boolean isStreaming) {
		firePropertyChange("streaming", this.isStreaming, Boolean.valueOf(isStreaming));
		this.isStreaming = isStreaming;
	}

	public boolean isTitleChanged() {
		return isTitleChanged;
	}

	public void setTitleChanged(boolean isTitleChanged) {
		this.isTitleChanged = isTitleChanged;
	}

	public boolean isShowingLoadingIcon() {
		return isShowingLoadingIcon;
	}

	public void setShowingLoadingIcon(boolean isShowingLoadingIcon) {
		this.isShowingLoadingIcon = isShowingLoadingIcon;
	}

	public boolean isEventCreated() {
		return isEventCreated;
	}

	public void setEventCreated(boolean isEventCreated) {
		this.isEventCreated = isEventCreated;
	}
}
