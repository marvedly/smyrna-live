package se.smyrna.smyrnalive.model;

public class TimeInstance {
	private int hour;
	private int minute;
	
	public TimeInstance() {
		this(0,0);
	}
	
	public TimeInstance(int hour, int minute) {
		this.hour = hour;
		this.minute = minute;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}
	
	
}
