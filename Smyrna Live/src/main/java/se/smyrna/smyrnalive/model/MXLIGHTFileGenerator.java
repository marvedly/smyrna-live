package se.smyrna.smyrnalive.model;

import java.io.OutputStream;
import java.io.PrintWriter;

import se.smyrna.smyrnalive.utils.FileUtils;


public class MXLIGHTFileGenerator {
	private static MXLIGHTFileGenerator mxlightFileGenerator = null;
	private static String absolutePath;
	
	private MXLIGHTFileGenerator() {};
	
	public static synchronized MXLIGHTFileGenerator getInstance() {
		if (mxlightFileGenerator == null) {
			mxlightFileGenerator = new MXLIGHTFileGenerator();
		}
		return mxlightFileGenerator;
		
	}
	
	/**
	 * Get a file name for the YouTube properties xml-file.
	 * <p>
	 * NOTE: Only call once since a new name will be generated each time.
	 * Use getLastGeneratedXMLFileName() instead to get the last generated
	 * file name.
	 * @return a string containing the file name
	 */
	public String generateFMLE_XMLFileName(String folder, String id) {
		if (!folder.endsWith("\\")) {
			folder = folder.concat("\\");
		}
		absolutePath = folder.concat("youtube_fmle_profile_primary_" + id + ".xml");
		if (!FileUtils.isFilenameValid(absolutePath, "")) {
			throw new RuntimeException("Filens s�kv�g �r fel!!");
		};
		return absolutePath;
	}
	
	/**
	 * Get the last generated file name for the YouTube properties xml-file.
	 * @return a string containing the last generated file name
	 * @throws NullPointerException thrown if generateXMLFileName()
	 * 			is never called before.
	 */
	public String getLastGeneratedFMLE_XMLFileName() {
		return absolutePath;
	}
	
	
	public void generateFMLE_XMLFile(OutputStream outputStream, String streamName) {
		PrintWriter pw = new PrintWriter(outputStream);
		pw.println("<?xml version=\"1.0\" encoding=\"UTF-16\"?>");
		pw.println("<flashmedialiveencoder_profile>");
		pw.println("  <preset>");
		pw.println("    <name>YouTube (1080p Stream)</name>");
		pw.println("    <description></description>");
		pw.println("  </preset>");
		pw.println("  <capture>");
		pw.println("    <video>");
		pw.println("      <frame_rate>30.00</frame_rate>");
		pw.println("      <size>");
		pw.println("        <width>1920</width>");
		pw.println("        <height>1080</height>");
		pw.println("      </size>");
		pw.println("    </video>");
		pw.println("    <audio>");
		pw.println("      <sample_rate>44100</sample_rate>");
		pw.println("      <channels>2</channels>");
		pw.println("      <input_volume>75</input_volume>");
		pw.println("    </audio>");
		pw.println("  </capture>");
		pw.println("  <process>");
		pw.println("    <video>");
		pw.println("      <preserve_aspect></preserve_aspect>");
		pw.println("    </video>");
		pw.println("  </process>");
		pw.println("  <encode>");
		pw.println("    <video>");
		pw.println("      <format>H.264</format>");
		pw.println("      <datarate>6000</datarate>");
		pw.println("      <outputsize>1920x1080</outputsize>");
		pw.println("      <advanced>");
		pw.println("        <profile>Main</profile>");
		pw.println("        <level>4.1</level>");
		pw.println("        <keyframe_frequency>2 Seconds</keyframe_frequency>");
		pw.println("      </advanced>");
		pw.println("      <autoadjust>");
		pw.println("        <enable>false</enable>");
		pw.println("        <maxbuffersize>1</maxbuffersize>");
		pw.println("        <dropframes>");
		pw.println("          <enable>false</enable>");
		pw.println("        </dropframes>");
		pw.println("        <degradequality>");
		pw.println("          <enable>true</enable>");
		pw.println("          <minvideobitrate></minvideobitrate>");
		pw.println("          <preservepfq>false</preservepfq>");
		pw.println("        </degradequality>");
		pw.println("      </autoadjust>");
		pw.println("    </video>");
		pw.println("    <audio>");
		pw.println("      <format>AAC</format>");
		pw.println("      <datarate>192</datarate>");
		pw.println("    </audio>");
		pw.println("  </encode>");
		pw.println("  <output>");
		pw.println("    <rtmp>");
		pw.println("      <url><![CDATA[rtmp://a.rtmp.youtube.com/live2]]></url>");
		pw.println("      <stream><![CDATA[" + streamName + "]]></stream>");
		pw.println("    </rtmp>");
		pw.println("  </output>");
		pw.println("  <preview>");
		pw.println("    <video>");
		pw.println("      <input>");
		pw.println("        <zoom>100%</zoom>");
		pw.println("      </input>");
		pw.println("      <output>");
		pw.println("        <zoom>100%</zoom>");
		pw.println("      </output>");
		pw.println("    </video>");
		pw.println("    <audio></audio>");
		pw.println("  </preview>");
		pw.println("</flashmedialiveencoder_profile>");
		pw.close();
	}
	
	public void generateStreamsCDAFile(OutputStream outputStream, String streamName) {
		PrintWriter pw = new PrintWriter(outputStream);
		pw.println("<MXL.");
		pw.println("	<streams.");
		pw.println("		<YouTube (1080p Stream).");
		pw.println("			<connection.");
		pw.println("				auto reconnect.[TB32] = T");
		pw.println("				auto reconnect pause (s).[TS32] = 10");
		pw.println("				initialise min time (s).[TS32] = 8");
		pw.println("				initialise retry count.[TS32] = 0");
		pw.println("				full stream reinit.[TB32] = T");
		pw.println("			>");
		pw.println("			<user vars.");
		pw.println("				Type.[TSTR8] = \"rtmp://\"");
		pw.println("				Server.[TSTR8] = \"a.rtmp.youtube.com\"");
		pw.println("				Application.[TSTR8] = \"live2\"");
		pw.println("				Play path.[TSTR8] = \"" + streamName + "\"");
		pw.println("				Authentication.[TSTR8] = \"|UV:Server|\"");
		pw.println("				Username.[TSTR8] = \"username\"");
		pw.println("				Password.[TSTR8] = \"password\"");
		pw.println("				Video mode.[TSTR8] = \"-c:v copy -copyts -r |INstream-fps|\"");
		pw.println("				Video crop.[TSTR8] = \"-s |UV:Video size X|x|UV:Video size Y|\"");
		pw.println("				Video crop-left.[TSTR8] = \"0\"");
		pw.println("				Video crop-right.[TSTR8] = \"0\"");
		pw.println("				Video crop-top.[TSTR8] = \"0\"");
		pw.println("				Video crop-bottom.[TSTR8] = \"0\"");
		pw.println("				Video logo.[TSTR8] = \" \"");
		pw.println("				Video logo-x.[TSTR8] = \"10\"");
		pw.println("				Video logo-y.[TSTR8] = \"10\"");
		pw.println("				Video logo-video file.[TSTR8] = \"img/logo video overlay.mov\"");
		pw.println("				Video logo-image file.[TSTR8] = \"img\\\\logo image overlay.png\"");
		pw.println("				Video size X.[TSTR8] = \"1920\"");
		pw.println("				Video size Y.[TSTR8] = \"1080\"");
		pw.println("				Video Bitrate (Kbps).[TSTR8] = \"6000\"");
		pw.println("				Video H264 Profile.[TSTR8] = \"-profile:v high\"");
		pw.println("				FPS divider.[TSTR8] = \"1\"");
		pw.println("				Keyframe interval (s).[TSTR8] = \"2\"");
		pw.println("				Audio Volume adjust (dB).[TSTR8] = \"0\"");
		pw.println("				Audio Codec.[TSTR8] = \"-c:a libvo_aacenc\"");
		pw.println("				Audio Channels.[TSTR8] = \"-ac 2\"");
		pw.println("				Audio Bitrate (Kbps).[TSTR8] = \"196\"");
		pw.println("				Audio Sample rate (Hz).[TSTR8] = \"44100\"");
		pw.println("				--Buffer size.[TSTR8] = \"67108864\"");
		pw.println("				--Client buffer time (ms).[TSTR8] = \"1000\"");
		pw.println("				--Input Mode.[TSTR8] = \"-rtbufsize |UV:--Buffer size| -re -f mpegts -c:v:0 h264 -c:a:1 aac -analyzeduration 500000 -fpsprobesize 6\"");
		pw.println("				<opts.");
		pw.println("					<Type.");
		pw.println("						RTMP.[TSTR8] = \"rtmp://\"");
		pw.println("						RTMPS.[TSTR8] = \"rtmps://\"");
		pw.println("						RTMPE.[TSTR8] = \"rtmpe://\"");
		pw.println("					>");
		pw.println("					<Video mode.");
		pw.println("						HARDWARE DIRECT.[TSTR8] = \"-c:v copy -copyts -r |INstream-fps|\"");
		pw.println("						RE-ENCODE.[TSTR8] = \"|UV:Video logo| -vsync cfr -c:v libx264 |UV:Video H264 Profile| -b:v |UV:Video Bitrate (Kbps)|k |UV:Video crop| -g |(|Floor(|UV:Keyframe interval (s)|*(|INstream-fps|/|UV:FPS divider|))|)| |UV:Audio Codec| -maxrate |(|(|UV:Video Bitrate (Kbps)|*1000)+(|UV:Audio Bitrate (Kbps)|*1000)|)| -r |(|(|INstream-fps|/|UV:FPS divider|)|)|\"");
		pw.println("					>");
		pw.println("					<Authentication.");
		pw.println("						OFF.[TSTR8] = \"|UV:Server|\"");
		pw.println("						ON.[TSTR8] = \"|UV:Username|:|UV:Password|@|UV:Server|\"");
		pw.println("					>");
		pw.println("					<Video H264 Profile.");
		pw.println("						baseline.[TSTR8] = \"-profile:v baseline\"");
		pw.println("						main.[TSTR8] = \"-profile:v main\"");
		pw.println("						high.[TSTR8] = \"-profile:v high\"");
		pw.println("					>");
		pw.println("					<Video crop.");
		pw.println("						ON.[TSTR8] = \"-vf \\\"crop=|INstream-V-X|-|(||UV:Video crop-left|+|UV:Video crop-right||)|:|INstream-V-Y|-|(||UV:Video crop-top|+|UV:Video crop-bottom||)|:|UV:Video crop-left|:|UV:Video crop-bottom|,scale=|UV:Video size X|x|UV:Video size Y|\\\"\"");
		pw.println("						OFF.[TSTR8] = \"-s |UV:Video size X|x|UV:Video size Y|\"");
		pw.println("					>");
		pw.println("					<Video logo.");
		pw.println("						ON : Image overlay.[TSTR8] = \"-loop 1 -i \\\"|UV:Video logo-image file|\\\" -filter_complex overlay=\\\"|UV:Video logo-x|:|UV:Video logo-y|\\\"\"");
		pw.println("						ON : Video overlay.[TSTR8] = \"-f lavfi -r |INstream-fps| -i movie=\\\"|UV:Video logo-video file|\\\":loop=0 -filter_complex overlay=\\\"|UV:Video logo-x|:|UV:Video logo-y|\\\"\"");
		pw.println("						OFF.[TSTR8] = \" \"");
		pw.println("					>");
		pw.println("					<Audio Codec.");
		pw.println("						mp3.[TSTR8] = \"-c:a mp3\"");
		pw.println("						aac (ffmpeg).[TSTR8] = \"-strict experimental -c:a aac\"");
		pw.println("						aac (visualON).[TSTR8] = \"-c:a libvo_aacenc\"");
		pw.println("					>");
		pw.println("					<Audio Channels.");
		pw.println("						mono.[TSTR8] = \"-ac 1\"");
		pw.println("						stereo.[TSTR8] = \"-ac 2\"");
		pw.println("					>");
		pw.println("					<--Input Mode.");
		pw.println("						default.[TSTR8] = \"-rtbufsize |UV:--Buffer size| -re -f mpegts -c:v:0 h264 -c:a:1 aac -analyzeduration 800000 -fpsprobesize 10\"");
		pw.println("						low latency.[TSTR8] = \"-rtbufsize |UV:--Buffer size| -re -f mpegts -c:v:0 h264 -c:a:1 aac -analyzeduration 500000 -fpsprobesize 6\"");
		pw.println("						lowest latency.[TSTR8] = \"-rtbufsize |UV:--Buffer size| -f mpegts -c:v:0 h264 -c:a:1 aac -analyzeduration 340000 -fpsprobesize 4\"");
		pw.println("						ultra low latency.[TSTR8] = \"-rtbufsize |UV:--Buffer size| -re -f mpegts -c:v:0 h264 -c:a:1 aac -analyzeduration 240000 -fpsprobesize 3\"");
		pw.println("					>");
		pw.println("				>");
		pw.println("			>");
		pw.println("			command line.[TSTR8] = \"|FPE|\\\"|MXL|utils\\\\ffmpeg.exe\\\" |UV:--Input Mode| -i \\\"tcp://|INstream-ip|:|INstream-port|\\\" |UV:Video mode| -af \\\"volume=|UV:Audio Volume adjust (dB)|dB\\\" |UV:Audio Codec| |UV:Audio Channels| -ar |UV:Audio Sample rate (Hz)| -b:a |UV:Audio Bitrate (Kbps)|k -bufsize 2000k -f flv - |RUN PIPED|\\\"|MXL|utils\\\\ffmpeg.exe\\\" -f flv -i - -c:v copy -copyts -c:a copy -f flv -rtmp_buffer |UV:--Client buffer time (ms)| -rtmp_app \\\"|UV:Application|\\\" -rtmp_playpath \\\"|UV:Play path|\\\" \\\"|UV:Type||UV:Authentication|\\\"\"");
		pw.println("			short description.[TSTR8] = \"YouTube (1080p Stream)\"");
		pw.println("			description.[TSTR8] = \"YouTube (1080p Stream)\\r\\nimported from FME/FMLE xml streaming profile 'youtube_fmle_profile_primary_fd_nOjnDjFY.xml' based on 'RTMP.mxs' template)\"");
		pw.println("			helpURL.[TSTR8] = \"http://mxlight.co.uk/stream-preset-help---rtmp.html\"");
		pw.println("		>");
		pw.println("	>");
		pw.println(">");
		pw.close();
	}
}
