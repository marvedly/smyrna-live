package se.smyrna.smyrnalive.model.events;

import se.smyrna.smyrnalive.model.ProgramSettings;

public class SwedishService implements DescriptableIEvent {
	private String tags;
	
	@Override
	public String getName() {
		return "Gudstj�nst";
	}

	@Override
	public String getTags() {
		return tags;
	}

	@Override
	public void setTags(String tags) {
		this.tags = tags;
	}

	@Override
	public String getThumbnailLocation() {
		return ProgramSettings.getInstance().getProgramResourceLocation() + "\\images\\thumbnail swedish.png";
	}

	@Override
	public String getDescription_sermon() {
		return "Predikan";
	}

	@Override
	public String getDescription_nbr() {
		return "Nr";
	}

	@Override
	public String getDescription_of() {
		return "av";
	}

	@Override
	public String getDescription_in_series() {
		return "i m�tesserien";
	}

	@Override
	public String getDescription_Smyrna() {
		return "Smyrnakyrkan G�teborg";
	}

}
