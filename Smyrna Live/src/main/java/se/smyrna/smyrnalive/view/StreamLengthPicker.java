package se.smyrna.smyrnalive.view;

import java.util.ResourceBundle;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import se.smyrna.smyrnalive.model.ProgramSettings;
import se.smyrna.smyrnalive.model.TimeInstance;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Composite;

public class StreamLengthPicker extends Dialog {

	protected Shell shell;
	private Spinner spinnerHours;
	private Spinner spinnerMinutes;
	private String hours;
	private String minutes;
	private final String BUNDLE_NAME = "se.smyrna.smyrnalive.locale.messagesDialog";

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public StreamLengthPicker(Shell parent, int style) {
		super(parent, style);
		setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("StreamLengthPicker_this_text"));
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public TimeInstance open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		if (hours == null || minutes == null || !(hours.matches("\\d+") && minutes.matches("\\d+") )) {
			return null;
		}
		return new TimeInstance(Integer.parseInt(hours), Integer.parseInt(minutes));
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shell = new Shell(getParent(), SWT.DIALOG_TRIM);
		shell.setSize(286, 125);
		shell.setText(getText());
		
		Composite composite = new Composite(shell, SWT.NONE);
		composite.setBounds(19, 10, 241, 80);
		composite.setLayout(null);
		
		spinnerHours = new Spinner(composite, SWT.BORDER);
		spinnerHours.setBounds(10, 10, 54, 22);
		spinnerHours.setMaximum(9999);
		spinnerHours.setSelection(ProgramSettings.getInstance().getDefaultStreamingLength().getHour());
		
		Button btnOk = new Button(composite, SWT.NONE);
		btnOk.setBounds(83, 46, 75, 25);
		btnOk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				hours = spinnerHours.getText();
				minutes = spinnerMinutes.getText();
				getParent().setEnabled(true);
				shell.close();
			}
		});
		btnOk.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("StreamLengthPicker_btnOk_text"));
		shell.setDefaultButton(btnOk);
		
		shell.addListener(SWT.Close, new Listener() {
			@Override
			public void handleEvent(Event event) {
				getParent().setEnabled(true);
				event.doit = true;
			}
		});
		
		Label lblHoursAnd = new Label(composite, SWT.NONE);
		lblHoursAnd.setBounds(70, 13, 62, 15);
		lblHoursAnd.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("StreamLengthPicker_lblHoursAnd_text"));
		
		spinnerMinutes = new Spinner(composite, SWT.BORDER);
		spinnerMinutes.setBounds(138, 10, 41, 22);
		spinnerMinutes.setMaximum(59);
		spinnerMinutes.setSelection(ProgramSettings.getInstance().getDefaultStreamingLength().getMinute());
		
		Label lblMinutes = new Label(composite, SWT.NONE);
		lblMinutes.setBounds(185, 13, 42, 15);
		lblMinutes.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("StreamLengthPicker_lblMinutes_text"));

	}
}
