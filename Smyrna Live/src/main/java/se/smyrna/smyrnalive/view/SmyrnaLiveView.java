package se.smyrna.smyrnalive.view;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import se.smyrna.smyrnalive.model.ProgramSettings;
import se.smyrna.smyrnalive.model.SmyrnaLive;
import se.smyrna.smyrnalive.utils.DateUtils;

public class SmyrnaLiveView implements PropertyChangeListener {

	public final String BUNDLE_NAME = "se.smyrna.smyrnalive.locale.messages";
	private SmyrnaLive smyrnaLive;
	protected Shell shell;
	private Button radioSwedishService;
	private Button radioInternationalService;
	private Button radioFarsiService;
	private Button radioOther;
	private Button radioNormalSettings;
	private Button radioAdvancedSettings;
	private Text textTopic;
	private Text textDescription;
	private Combo comboPreacher;
	private Button btnGoLive;

	private MenuItem mntmSettingsMenu;
	private MenuItem mntmSetXMLfolderPath;
	private MenuItem mntmSetDefaultStreamLength;
	private MenuItem mntmLanguages;
	private MenuItem mntmEnglish;
	private MenuItem mntmSwedish;

	private Label lblStartTime;
	private Label lblEndTime;
	private Label lblPrivacyStatus;
	private DateTime dtStartDate;
	private DateTime dtStartTime;
	private DateTime dtEndDate;
	private DateTime dtEndTime;
	private Combo comboPrivacyStatus;
	private Text textTags;
	private Label lblTags;
	private Button cbOverwriteExisting;
	private Display display;
	private Text textTitlePreview;
	private Label lblPreacher;
	private Label lblPreachingTopic;
	private Label lblIncorrect;
	private Button btnStopRecording;
	private Button btnStartRecording;
	private Button btnStopStreaming;
	private Canvas canvasLoading;
	private ImageLoader loader;
	private Image loadingImage;
	private GC gc;
	private Composite compositeBtnGoLive;
	private Combo comboSeries;
	private Combo comboPart;
	private Combo comboPartAll;
	private Label lblSeries;
	private Label lblPart;
	private Label lblOf;
	private Label lblTitle;
	private Label lblDescription;
	private Label lblMxlight;
	private Composite compositeProfiles;



	/**
	 * @param mainModel 
	 * @wbp.parser.entryPoint
	 */
	public SmyrnaLiveView(SmyrnaLive smyrnaLive) {
		this.smyrnaLive = smyrnaLive;
		createContents();
	}

	/**
	 * Open the window.
	 */
	public void open() {
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		display = Display.getDefault();
		shell = new Shell(display, SWT.TITLE | SWT.MIN | SWT.CLOSE | SWT.BORDER | SWT.APPLICATION_MODAL);
		shell.setSize(730, 430);
		

		Menu menu = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menu);

		mntmSettingsMenu = new MenuItem(menu, SWT.CASCADE);

		Menu menuSettings = new Menu(mntmSettingsMenu);
		mntmSettingsMenu.setMenu(menuSettings);

		mntmSetXMLfolderPath = new MenuItem(menuSettings, SWT.NONE);
		mntmSetXMLfolderPath.setData("xmlPath");

		mntmSetDefaultStreamLength = new MenuItem(menuSettings, SWT.NONE);
		mntmSetDefaultStreamLength.setData("streamLength");

		mntmLanguages = new MenuItem(menu, SWT.CASCADE);
		

		Menu menuLanguages = new Menu(mntmLanguages);
		mntmLanguages.setMenu(menuLanguages);
		
				mntmSwedish = new MenuItem(menuLanguages, SWT.NONE);
				mntmSwedish.setData("swedish");

		mntmEnglish = new MenuItem(menuLanguages, SWT.NONE);
		mntmEnglish.setData("english");

		compositeProfiles = new Composite(shell, SWT.NONE);
		compositeProfiles.setBounds(10, 10, 245, 82);
		compositeProfiles.setData("compositeProfiles");

		radioSwedishService = new Button(compositeProfiles, SWT.RADIO);
		radioSwedishService.setBounds(10, 0, 196, 16);
		radioSwedishService.setData("radioSwedishService");

		radioInternationalService = new Button(compositeProfiles, SWT.RADIO);
		radioInternationalService.setBounds(10, 22, 196, 16);
		radioInternationalService.setData("radioInternationalService");

		radioFarsiService = new Button(compositeProfiles, SWT.RADIO);
		radioFarsiService.setBounds(10, 44, 196, 16);
		radioFarsiService.setData("radioFarsiService");

		radioOther = new Button(compositeProfiles, SWT.RADIO);
		radioOther.setBounds(10, 66, 196, 16);
		radioOther.setData("radioOther");

		Composite compositeSettings = new Composite(shell, SWT.NONE);
		compositeSettings.setBounds(10, 104, 245, 267);

		radioNormalSettings = new Button(compositeSettings, SWT.RADIO);
		radioNormalSettings.setSelection(true);
		radioNormalSettings.setBounds(10, 10, 235, 16);
		radioNormalSettings.setData("radioNormal");

		radioAdvancedSettings = new Button(compositeSettings, SWT.RADIO);
		radioAdvancedSettings.setBounds(10, 32, 197, 16);
		radioAdvancedSettings.setData("radioAdvanced");

		Composite compositeAdvanced = new Composite(compositeSettings, SWT.NONE);
		compositeAdvanced.setBounds(0, 54, 245, 213);

		lblStartTime = new Label(compositeAdvanced, SWT.RIGHT);
		lblStartTime.setAlignment(SWT.LEFT);
		lblStartTime.setBounds(10, 15, 85, 15);

		dtStartTime = new DateTime(compositeAdvanced, SWT.BORDER | SWT.TIME | SWT.SHORT);
		dtStartTime.setBounds(101, 10, 49, 24);

		dtStartDate = new DateTime(compositeAdvanced, SWT.BORDER);
		dtStartDate.setBounds(156, 10, 80, 24);
		dtStartDate.setData("dtStartDate");

		lblEndTime = new Label(compositeAdvanced, SWT.NONE);
		lblEndTime.setBounds(10, 45, 85, 15);

		dtEndTime = new DateTime(compositeAdvanced, SWT.BORDER | SWT.TIME | SWT.SHORT);
		dtEndTime.setBounds(101, 40, 49, 24);

		dtEndDate = new DateTime(compositeAdvanced, SWT.BORDER);
		dtEndDate.setBounds(156, 40, 80, 24);

		lblPrivacyStatus = new Label(compositeAdvanced, SWT.NONE);
		lblPrivacyStatus.setBounds(10, 73, 85, 15);

		comboPrivacyStatus = new Combo(compositeAdvanced, SWT.READ_ONLY);
		
		comboPrivacyStatus.setBounds(101, 70, 135, 23);

		lblTags = new Label(compositeAdvanced, SWT.WRAP);
		lblTags.setBounds(10, 102, 85, 62);

		textTags = new Text(compositeAdvanced, SWT.BORDER | SWT.WRAP);
		textTags.setBounds(101, 99, 135, 85);
		textTags.setData("textTags");

		cbOverwriteExisting = new Button(compositeAdvanced, SWT.CHECK);
		cbOverwriteExisting.setBounds(10, 197, 235, 16);
		cbOverwriteExisting.setData("cbOverwriteExisting");

		Composite compositeText = new Composite(shell, SWT.NONE);
		compositeText.setBounds(282, 10, 432, 361);

		lblTitle = new Label(compositeText, SWT.NONE);
		lblTitle.setBounds(79, 13, 26, 15);

		textTitlePreview = new Text(compositeText, SWT.BORDER);
		textTitlePreview.setBounds(111, 10, 311, 21);
		textTitlePreview.setData("textTitlePreview");

		lblPreachingTopic = new Label(compositeText, SWT.NONE);
		lblPreachingTopic.setAlignment(SWT.RIGHT);
		lblPreachingTopic.setBounds(10, 40, 93, 15);

		textTopic = new Text(compositeText, SWT.BORDER);
		textTopic.setBounds(111, 37, 311, 21);
		textTopic.setData("textTopic");

		lblPreacher = new Label(compositeText, SWT.RIGHT);
		lblPreacher.setBounds(10, 67, 95, 15);

		comboPreacher = new Combo(compositeText, SWT.NONE);
		comboPreacher.setBounds(111, 64, 311, 21);
		comboPreacher.setData("textPreacher");
		

		lblSeries = new Label(compositeText, SWT.NONE);
		lblSeries.setAlignment(SWT.RIGHT);
		lblSeries.setBounds(26, 96, 79, 15);

		comboSeries = new Combo(compositeText, SWT.BORDER);
		comboSeries.setData("textSeries");
		comboSeries.setBounds(111, 93, 163, 23);

		lblPart = new Label(compositeText, SWT.NONE);
		lblPart.setAlignment(SWT.RIGHT);
		lblPart.setBounds(280, 96, 26, 15);

		comboPart = new Combo(compositeText, SWT.BORDER);
		comboPart.setItems(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"});
		comboPart.setData("textPart");
		comboPart.setBounds(312, 93, 41, 23);

		lblOf = new Label(compositeText, SWT.NONE);
		lblOf.setAlignment(SWT.CENTER);
		lblOf.setBounds(359, 96, 16, 15);

		comboPartAll = new Combo(compositeText, SWT.BORDER);
		comboPartAll.setItems(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"});
		comboPartAll.setData("textPartAll");
		comboPartAll.setBounds(381, 93, 41, 23);
		
		setComboValuesReadFromFiles();

		lblDescription = new Label(compositeText, SWT.RIGHT);
		lblDescription.setBounds(42, 121, 63, 15);

		textDescription = new Text(compositeText, SWT.BORDER | SWT.WRAP | SWT.MULTI);
		textDescription.setData("textDescription");
		textDescription.setBounds(111, 122, 311, 121);

		lblIncorrect = new Label(compositeText, SWT.NONE);
		lblIncorrect.setAlignment(SWT.RIGHT);
		lblIncorrect.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblIncorrect.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		lblIncorrect.setVisible(false);
		lblIncorrect.setBounds(111, 254, 174, 15);

		compositeBtnGoLive = new Composite(compositeText, SWT.NONE);
		compositeBtnGoLive.setBounds(291, 249, 131, 25);

		canvasLoading = new Canvas(compositeBtnGoLive, SWT.NONE);
		canvasLoading.setLocation(57, 7);
		canvasLoading.setSize(16, 11);
		canvasLoading.setVisible(false);

		btnGoLive = new Button(compositeBtnGoLive, SWT.NONE);
		btnGoLive.setBounds(0, 0, 131, 25);
		btnGoLive.setData("btnGoLive");
		
		lblMxlight = new Label(compositeText, SWT.CENTER);
		lblMxlight.setBounds(27, 295, 102, 15);

		Label separator3 = new Label(compositeText, SWT.SEPARATOR | SWT.HORIZONTAL);
		separator3.setBounds(10, 302, 412, 2);

		Composite compositeMXLight = new Composite(compositeText, SWT.NONE);
		compositeMXLight.setBounds(10, 310, 412, 41);

		btnStopRecording = new Button(compositeMXLight, SWT.NONE);
		btnStopRecording.setEnabled(false);
		btnStopRecording.setImage(SWTResourceManager.getImage(SmyrnaLiveView.class, "/pictures/stopRecord14x14.png"));
		btnStopRecording.setBounds(140, 10, 131, 25);
		btnStopRecording.setData("btnStopRecording");
		
		btnStartRecording = new Button(compositeMXLight, SWT.NONE);
		btnStartRecording.setImage(SWTResourceManager.getImage(SmyrnaLiveView.class, "/pictures/Record12x12.png"));
		btnStartRecording.setBounds(281, 10, 131, 25);
		btnStartRecording.setData("btnStartRecording");
		
		btnStopStreaming = new Button(compositeMXLight, SWT.NONE);
		btnStopStreaming.setEnabled(false);
		btnStopStreaming.setImage(SWTResourceManager.getImage(SmyrnaLiveView.class, "/pictures/streamStop14x14.png"));
		btnStopStreaming.setData("btnStopStreaming");
		btnStopStreaming.setBounds(0, 10, 131, 25);
		loader = new ImageLoader();
		loader.load(ClassLoader.getSystemResourceAsStream("pictures/loading.gif"));
		loadingImage = new Image(display, loader.data[0]);
		gc = new GC(loadingImage);

		// Set time according to those in the file
		int startHour = ProgramSettings.getInstance().getStartHour();
		int startMin = ProgramSettings.getInstance().getStartMin();
		if (startHour != -1 && startMin != -1) {
			dtStartTime.setHours(startHour);
			dtStartTime.setMinutes(startMin);
		}
		
		setTextToButtons();
		updateEndTimeToDefault();

		Label label = new Label(shell, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setBounds(9, 96, 245, 2);

		Label separator = new Label(shell, SWT.SEPARATOR | SWT.VERTICAL);
		separator.setBounds(274, 10, 2, 361);

		enableAdvancedSettings(false);
	}
	
	public void setTextToButtons() {
		shell.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_shell_text"));
		mntmSettingsMenu.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_mntmSettingsMenu_text"));
		mntmSetXMLfolderPath.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_mntmSetXMLfolderPath_text"));

		mntmSetDefaultStreamLength.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_mntmSetDefaultStreamLength_text"));
		mntmLanguages.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_mntmLanguages_text"));
		mntmEnglish.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_mntmEnglish_text"));
		mntmSwedish.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_mntmSwedish_text"));
		compositeProfiles.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_compositeProfiles_toolTipText"));
		radioSwedishService.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_radioSwedishService_text"));
		radioSwedishService.setToolTipText(radioSwedishService.getParent().getToolTipText());
		radioInternationalService.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_radioInternationalService_text"));
		radioInternationalService.setToolTipText(radioInternationalService.getParent().getToolTipText());
		radioFarsiService.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_radioFarsiService_text"));
		radioFarsiService.setToolTipText(radioFarsiService.getParent().getToolTipText());
		radioOther.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_radioOther_text"));
		radioOther.setToolTipText(radioOther.getParent().getToolTipText());
		radioNormalSettings.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_radioNormalSettings_text"));
		radioAdvancedSettings.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_radioAdvancedSettings_text"));
		lblStartTime.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_lblStartTime_text"));
		dtStartTime.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_dtStartTime_toolTipText"));
		dtStartDate.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_dtStartDate_toolTipText"));
		lblEndTime.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_lblEndTime_text"));
		dtEndTime.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_dtEndTime_toolTipText"));
		dtEndDate.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_dtEndDate_toolTipText"));
		lblPrivacyStatus.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_lblPrivacyStatus_text"));
		comboPrivacyStatus.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_comboPrivacyStatus_toolTipText"));
		comboPrivacyStatus.setItems(new String[] {ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_comboPrivacyStatus_Public"),
				ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_comboPrivacyStatus_Unlisted"), ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_comboPrivacyStatus_Private")});
		comboPrivacyStatus.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_comboPrivacyStatus_Public"));
		lblTags.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_lblTags_text"));
		textTags.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_textTags_toolTipText"));
		cbOverwriteExisting.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_cbOverwriteExisting_toolTipText"));
		cbOverwriteExisting.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_cbOverwriteExisting_text"));
		lblTitle.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_lblTitle_text"));
		textTitlePreview.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_textTitlePreview_toolTipText"));
		lblPreachingTopic.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_lblPreachingTopic_text"));
		lblPreacher.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_lblPreacher_text"));
		lblSeries.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_lblSeries_text"));
		lblPart.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_lblPart_text"));
		lblOf.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_lblOf_text"));
		lblDescription.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_lblDescription_text"));
		textDescription.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_textDescription_toolTipText"));
		lblIncorrect.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_lblIncorrect_text"));
		if (!smyrnaLive.isEventCreated()) {
			btnGoLive.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_btnGoLive_toolTipText"));
			btnGoLive.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_btnGoLive_text"));
		} else {
			btnGoLive.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_btnGoLive_toolTipText2"));
			btnGoLive.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_btnGoLive_text2"));
		}
		lblMxlight.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_lblMxlight_text"));
		btnStopRecording.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_btnStopRecording_toolTipText"));
		btnStopRecording.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_btnStopRecording_text"));
		btnStartRecording.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_btnStartRecording_toolTipText"));
		btnStartRecording.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_btnStartRecording_text"));
		btnStopStreaming.setToolTipText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_btnStopStreaming_toolTipText"));
		btnStopStreaming.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("SmyrnaLiveView_btnStopStreaming_text"));

	}

	private void setComboValuesReadFromFiles() {
		ProgramSettings ps = ProgramSettings.getInstance();
		comboPreacher.setItems(ps.getPreachers());
		comboSeries.setItems(ps.getSeries());
	}
	
	public void enableAdvancedSettings(boolean enable) {
		lblStartTime.setEnabled(enable);
		dtStartTime.setEnabled(enable);
		dtStartDate.setEnabled(enable);
		lblEndTime.setEnabled(enable);
		dtEndTime.setEnabled(enable);
		dtEndDate.setEnabled(enable);
		lblPrivacyStatus.setEnabled(enable);
		comboPrivacyStatus.setEnabled(enable);
		lblTags.setEnabled(enable);
		textTags.setEnabled(enable);
		cbOverwriteExisting.setEnabled(enable);
		if (!radioOther.getSelection()) {
			textTitlePreview.setEnabled(enable);
		}
	}

	public void enableOther(boolean enable) {
		lblPreacher.setEnabled(!enable);
		comboPreacher.setEnabled(!enable);
		lblPreachingTopic.setEnabled(!enable);
		textTopic.setEnabled(!enable);
		lblSeries.setEnabled(!enable);
		comboSeries.setEnabled(!enable);
		lblPart.setEnabled(!enable);
		comboPart.setEnabled(!enable);
		lblOf.setEnabled(!enable);
		comboPartAll.setEnabled(!enable);
		textTitlePreview.setEnabled(enable);
	}

	public void enableSettings(boolean enable) {
		radioNormalSettings.setEnabled(enable);
		radioAdvancedSettings.setEnabled(enable);
	}

	public void updateEndTimeToDefault() {
		ProgramSettings ps = ProgramSettings.getInstance();
		int endMinute = dtStartTime.getMinutes() + ps.getDefaultStreamingLength().getMinute();
		int endHour = dtStartTime.getHours() + ps.getDefaultStreamingLength().getHour();	
		if (endMinute > 59) {
			endHour++;
		}
		dtEndTime.setMinutes(endMinute % 60);

		dtEndDate.setDate(dtStartDate.getYear(), dtStartDate.getMonth(), dtStartDate.getDay());
		while (endHour > 23) {
			DateUtils.increaseOneDayWithRespectToMonthAndYear(dtEndDate);
			endHour -= 24;
		}
		if (endHour < 0) {
			endHour += 24;
		}
		dtEndTime.setHours(endHour % 24);
	}

	public String getTimeYYMMDD() {
		return String.format("%02d%02d%02d", dtStartDate.getYear() % 100, dtStartDate.getMonth()+1, dtStartDate.getDay());
	}

	public Shell getShell() {
		return shell;
	}

	public Button getBtnGoLive() {
		return btnGoLive;
	}

	public Button getBtnStopRecording() {
		return btnStopRecording;
	}

	public Button getBtnStartRecording() {
		return btnStartRecording;
	}

	public Button getBtnStopStreaming() {
		return btnStopStreaming;
	}

	public Button getRadioSwedishService() {
		return radioSwedishService;
	}

	public Button getRadioInternationalService() {
		return radioInternationalService;
	}

	public Button getRadioFarsiService() {
		return radioFarsiService;
	}

	public Button getRadioOther() {
		return radioOther;
	}

	public Button getRadioNormalSettings() {
		return radioNormalSettings;
	}

	public Button getRadioAdvancedSettings() {
		return radioAdvancedSettings;
	}

	public MenuItem getMntmSetXMLfolderPath() {
		return mntmSetXMLfolderPath;
	}

	public MenuItem getMntmSetDefaultStreamLength() {
		return mntmSetDefaultStreamLength;
	}

	public MenuItem getMntmEnglish() {
		return mntmEnglish;
	}

	public MenuItem getMntmSwedish() {
		return mntmSwedish;
	}

	public Text getTextTopic() {
		return textTopic;
	}

	public Text getTextDescription() {
		return textDescription;
	}

	public Combo getComboPreacher() {
		return comboPreacher;
	}

	public Combo getComboSeries() {
		return comboSeries;
	}

	public Combo getComboPart() {
		return comboPart;
	}

	public Combo getComboPartAll() {
		return comboPartAll;
	}

	public DateTime getDtStartDate() {
		return dtStartDate;
	}

	public DateTime getDtStartTime() {
		return dtStartTime;
	}

	public DateTime getDtEndDate() {
		return dtEndDate;
	}

	public DateTime getDtEndTime() {
		return dtEndTime;
	}

	public Combo getComboPrivacyStatus() {
		return comboPrivacyStatus;
	}

	public Text getTextTitlePreview() {
		return textTitlePreview;
	}

	public Canvas getCanvasLoading() {
		return canvasLoading;
	}

	public ImageLoader getLoader() {
		return loader;
	}

	public Image getLoadingImage() {
		return loadingImage;
	}

	public GC getGc() {
		return gc;
	}

	public Text getTextTags() {
		return textTags;
	}

	public Label getLblIncorrect() {
		return lblIncorrect;
	}

	public Button getCbOverwriteExisting() {
		return cbOverwriteExisting;
	}

	@Override
	public void propertyChange(PropertyChangeEvent pce) {
		if (pce.getPropertyName().equals("title")) {
			textTitlePreview.setText((String)pce.getNewValue());
		} else if (pce.getPropertyName().equals("recording")) {
			btnStartRecording.setEnabled(!(boolean) pce.getNewValue());
			btnStopRecording.setEnabled((boolean) pce.getNewValue());
		} else if (pce.getPropertyName().equals("streaming")) {
			btnStopStreaming.setEnabled((boolean) pce.getNewValue());
		}
	}
}
