package se.smyrna.smyrnalive.utils;


import org.eclipse.swt.widgets.DateTime;

public class DateUtils {
	
	public static void increaseOneDayWithRespectToMonthAndYear(DateTime dt) {
		int day = dt.getDay() + 1;
		if (dt.getMonth() == 0
				|| dt.getMonth() == 2
				|| dt.getMonth() == 4
				|| dt.getMonth() == 6
				|| dt.getMonth() == 7
				|| dt.getMonth() == 9
				|| dt.getMonth() == 11) { // if 31 days
			if (day > 31) {
				int month = dt.getMonth() + 1;
				if (month > 11) {
					dt.setYear(dt.getYear() + 1);
				}
				dt.setMonth(month % 12);
			}
			if (day == 31) {
				dt.setDay(day);
			} else {
				dt.setDay(day % 31);
			}
		} else if (dt.getMonth() == 1) { // if 28 or 29 days
				int leapYear = isLeapYear(dt.getYear()) ? 1 : 0;
				int nbrOfDaysInMonth = 28 + leapYear;
				if (day > nbrOfDaysInMonth) {
					dt.setMonth(2);
				}
				if (day == nbrOfDaysInMonth) {
					dt.setDay(day);
				} else {
					dt.setDay(day % nbrOfDaysInMonth);
				}
		} else { // if 30 days
			if (day > 30) {
				dt.setMonth(dt.getMonth() + 1);
			}
			if (day == 30) {
				dt.setDay(day);
			} else {
				dt.setDay(day % 30);
			}
		}
	}
	
	/**
	 * Checks whether the given year is a leap year or not
	 * @param year a given year
	 * @return true if leap year, else false
	 */
	public static boolean isLeapYear(int year) {
		return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
	}
}
