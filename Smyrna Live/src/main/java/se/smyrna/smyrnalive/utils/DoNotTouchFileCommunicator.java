package se.smyrna.smyrnalive.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import se.smyrna.smyrnalive.model.ProgramSettings;

public class DoNotTouchFileCommunicator {
	private static String filePath = ProgramSettings.getInstance().DEFAULT_DO_NOT_TOUCH_FILE_LOCATION;

	private DoNotTouchFileCommunicator() {}
	
	public static void writeTitleAsItWasWhenStreamStartedToFile(String title) {
		File file = new File(filePath);
		BufferedWriter writer;
		try {
			if (file.exists()) {
				file.delete();
			} 
			file.createNewFile();

			writer = new BufferedWriter(new FileWriter(file));
			writer.append(title);
			writer.close();

		} catch (IOException e) {
			file.delete();
		}
	}

	public static String getTitleAsItWasWhenStreamStartedToFile() {
		File file = new File(filePath);
		BufferedReader reader;
		String title = null;
		try {
			if (file.exists()) {
				reader = new BufferedReader(new FileReader(file));
				title = reader.readLine();
				reader.close();
				file.delete();
			}
		} catch (IOException e) {
			file.delete();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		return title;

	}
}
