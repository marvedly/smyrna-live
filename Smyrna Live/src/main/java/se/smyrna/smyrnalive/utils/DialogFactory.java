package se.smyrna.smyrnalive.utils;

import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class DialogFactory {
	private final static String BUNDLE_NAME = "se.smyrna.smyrnalive.locale.messagesDialog";

	public static boolean createStopMXLightDialog(Shell parent, boolean isRecording, boolean isStreaming) {
		MessageBox dialog = new MessageBox(parent, SWT.ICON_WARNING | SWT.YES | SWT.NO);
		StringBuilder sb = new StringBuilder();
		sb.append(ResourceBundle.getBundle(BUNDLE_NAME).getString("StopMXLightDialog_current"));
		sb.append(" ");
		if (isRecording) {
			sb.append(ResourceBundle.getBundle(BUNDLE_NAME).getString("StopMXLightDialog_recording"));
			sb.append(" ");
			if (isStreaming) {
				sb.append(ResourceBundle.getBundle(BUNDLE_NAME).getString("StopMXLightDialog_and_streaming"));
				sb.append(" ");
			}
		} else {
			sb.append(ResourceBundle.getBundle(BUNDLE_NAME).getString("StopMXLightDialog_streaming"));
			sb.append(" ");
		}
		sb.append(ResourceBundle.getBundle(BUNDLE_NAME).getString("StopMXLightDialog_rest"));
		
		dialog.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("StopMXLightDialog_warning"));
		dialog.setMessage(sb.toString());
		int returnCode = dialog.open();
		return returnCode == SWT.YES;
	}

	public static boolean createGoPublicDialog(Shell parent) {
		MessageBox dialog = new MessageBox(parent, SWT.ICON_INFORMATION | SWT.YES | SWT.NO);
		dialog.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("GoPublicDialog_Text"));
		dialog.setMessage(ResourceBundle.getBundle(BUNDLE_NAME).getString("GoPublicDialog_Message"));
		int returnCode = dialog.open();
		return returnCode == SWT.YES;
	}
	
	public static boolean stopStreamingDialog(Shell parent) {
		MessageBox dialog = new MessageBox(parent, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
		dialog.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("StopStreamingDialog_Text"));
		dialog.setMessage(ResourceBundle.getBundle(BUNDLE_NAME).getString("StopStreamingDialog_Message"));
		int returnCode = dialog.open();
		return returnCode == SWT.YES;
	}

	public static boolean streamIsStillActiveDialog(Shell parent) {
		MessageBox dialog = new MessageBox(parent, SWT.ICON_WARNING | SWT.YES | SWT.NO);
		dialog.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("StreamIsStillActiveDialog_Text"));
		dialog.setMessage(ResourceBundle.getBundle(BUNDLE_NAME).getString("StreamIsStillActiveDialog_Message"));
		int returnCode = dialog.open();
		return returnCode == SWT.YES;
	}

	public static void internalErrorAtGoogleDialog(Shell parent) {
		MessageBox dialog = new MessageBox(parent, SWT.ICON_ERROR | SWT.OK);
		dialog.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("InternalErrorAtGoogle_Text"));
		dialog.setMessage(ResourceBundle.getBundle(BUNDLE_NAME).getString("InternalErrorAtGoogle_Message"));
		dialog.open();
	}
	
	public static void authDeniedDialog(Shell parent) {
		MessageBox dialog = new MessageBox(parent, SWT.ICON_ERROR | SWT.OK);
		dialog.setText(ResourceBundle.getBundle(BUNDLE_NAME).getString("AuthDenied_Text"));
		dialog.setMessage(ResourceBundle.getBundle(BUNDLE_NAME).getString("AuthDenied_Message"));
		dialog.open();
	}
}
