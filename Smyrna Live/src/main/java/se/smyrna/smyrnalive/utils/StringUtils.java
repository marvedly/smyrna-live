package se.smyrna.smyrnalive.utils;

public class StringUtils {
	public static String replaceIfNotEmpty(String target, String replacement) {
		if ("".equals(replacement)) {
			return target;
		} else {
			return target.replace(target, replacement);
		}
	}
}
