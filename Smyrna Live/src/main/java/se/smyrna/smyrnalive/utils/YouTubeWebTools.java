package se.smyrna.smyrnalive.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;

public class YouTubeWebTools
{
     public static void main(String args[])
     {
          HtmlPage page = null;
          boolean savePagesLocally = false;
          String url = "https://accounts.google.com/ServiceLogin?continue=https%3A%2F%2Fwww.youtube.com%2Fsignin%3Fnext%3D%252Fmy_live_events%253Faction_edit_live_event%253D1%2526event_id%253DZ_B_NsvTFjE%26feature%3Dredirect_login%26action_handle_signin%3Dtrue%26hl%3Dsv%26app%3Ddesktop&service=youtube&sacu=1&passive=1209600&acui=0#Email=martinhermansson92%40gmail.com";

          WebClient webClient = new WebClient();

          String savePagesLocallyString = System.getProperty("savePagesLocally");
          if(savePagesLocallyString != null )
          { savePagesLocally = Boolean.valueOf(savePagesLocallyString); }

          int pageNum = 1;
          String localFilePath = null;

          if(savePagesLocally)
          {
               localFilePath = System.getProperty("localFilePath");

               if(localFilePath == null)
               {
                    System.out.println("localFilePath property needs to be specified on command line, like so:" +
                         "-DlocalFilePath=somefilepath");
                    throw new RuntimeException("localFilePath property was not specified");
               }
               else
               {
                    String osName = System.getProperty("os.name");
                    String separator = null;

                    if(osName.indexOf(WINDOWS_OS) > -1)
                    { separator = "\\"; }
                    else // UNIX-style path
                    { separator = "/"; }

                    if( !localFilePath.endsWith(separator) )
                    { localFilePath += separator; }

                    // Create a new folder for local files- folder name is current date and time
                    SimpleDateFormat sd = new SimpleDateFormat("MM-dd-yyyy_HH_mm");
                    String formattedDate = sd.format(new Date());
                    localFilePath += formattedDate + separator;
                    File newLocalFolder = new File(localFilePath);
                    boolean success = newLocalFolder.mkdirs();

                    if(!success)
                    { throw new RuntimeException("Could not create new folder at location " + localFilePath); }

                }
          }

          try
          {
               page = webClient.getPage( url );

               System.out.println("Current page: YouTube");

               // Current page:
               // Title=YouTube
               // URL=https://accounts.google.com/ServiceLogin?service=youtube&uilel=3&hl=sv&continue=https%3A%2F%2Fwww.youtube.com%2Fsignin%3Fapp%3Ddesktop%26action_handle_signin%3Dtrue%26hl%3Dsv%26next%3D%252Fmy_live_events%253Faction_edit_live_event%253D1%2526event_id%253DZ_B_NsvTFjE%26feature%3Dredirect_login&passive=true

               HtmlInput checkbox3 = (HtmlInput) page.getElementById("PersistentCookie");
               checkbox3.click();

               HtmlPasswordInput passwordField4 = (HtmlPasswordInput) page.getElementById("Passwd");
               passwordField4.setValueAttribute("password");

               if( savePagesLocally )
               {
                    String fullPath = savePageLocally(page, localFilePath, pageNum);
                    pageNum++;
                    System.out.println("Page with title '" + page.getTitleText() + "' saved to " + fullPath);
               }

               HtmlElement theElement1 = (HtmlElement) page.getElementById("Email");               
               page = theElement1.click();

               if( savePagesLocally )
               {
                    String fullPath = savePageLocally(page, localFilePath, pageNum);
                    pageNum++;
                    System.out.println("Page with title '" + page.getTitleText() + "' saved to " + fullPath);
               }

               HtmlElement theElement2 = (HtmlElement) page.getElementById("Email");               
               page = theElement2.click();

               if( savePagesLocally )
               {
                    String fullPath = savePageLocally(page, localFilePath, pageNum);
                    pageNum++;
                    System.out.println("Page with title '" + page.getTitleText() + "' saved to " + fullPath);
               }

               HtmlElement theElement5 = (HtmlElement) page.getElementById("signIn");               
               page = theElement5.click();

               System.out.println("Current page: Google-konton");

               // Current page:
               // Title=Google-konton
               // URL=https://accounts.google.com/CheckCookie?hl=sv&checkedDomains=youtube&checkConnection=youtube%3A1078%3A1&pstMsg=1&chtml=LoginDoneHtml&service=youtube&continue=https%3A%2F%2Fwww.youtube.com%2Fsignin%3Fapp%3Ddesktop%26action_handle_signin%3Dtrue%26hl%3Dsv%26next%3D%252Fmy_live_events%253Faction_edit_live_event%253D1%2526event_id%253DZ_B_NsvTFjE%26feature%3Dredirect_login&gidl=CAA

               System.out.println("Current page: 150105 Farsi service - - YouTube");

               // Current page:
               // Title=150105 Farsi service - - YouTube
               // URL=https://www.youtube.com/my_live_events?action_edit_live_event=1&event_id=Z_B_NsvTFjE

               HtmlSelect selectField8 = (HtmlSelect) page.getElementById("country-select");
               List<HtmlOption> options9 = selectField8.getOptions();
               HtmlOption theOption10 = null;
               for(HtmlOption option: options9)
               {
                    if(option.getText().equals("Sverige") )
                    {
                         theOption10 = option;
                         break;
                    }
               }
               selectField8.setSelectedAttribute(theOption10, true );

               HtmlSelect selectField11 = (HtmlSelect) page.getElementById("tz-select");
               List<HtmlOption> options12 = selectField11.getOptions();
               HtmlOption theOption13 = null;
               for(HtmlOption option: options12)
               {
                    if(option.getText().equals("(GMT +01:00) Stockholm") )
                    {
                         theOption13 = option;
                         break;
                    }
               }
               selectField11.setSelectedAttribute(theOption13, true );

               HtmlSelect selectField16 = (HtmlSelect) page.getElementByName("category");
               List<HtmlOption> options17 = selectField16.getOptions();
               HtmlOption theOption18 = null;
               for(HtmlOption option: options17)
               {                    if(option.getText().equals("Utbildning") )
                    {
                         theOption18 = option;
                         break;
                    }
               }
               selectField16.setSelectedAttribute(theOption18, true );

               HtmlInput checkbox23 = (HtmlInput) page.getElementByName("make_private_on_complete");
               checkbox23.click();

               HtmlInput checkbox24 = (HtmlInput) page.getElementByName("allow_comments");
               checkbox24.click();

               HtmlInput checkbox25 = (HtmlInput) page.getElementByName("allow_ratings");
               checkbox25.click();

               if( savePagesLocally )
               {
                    String fullPath = savePageLocally(page, localFilePath, pageNum);
                    pageNum++;
                    System.out.println("Page with title '" + page.getTitleText() + "' saved to " + fullPath);
               }

               @SuppressWarnings("unchecked")
			List<HtmlElement> elements28 = (List<HtmlElement>) page.getByXPath(
                    "(//button)[20]");
               HtmlElement element29 = elements28.get(0);
               page = element29.click();

               if( savePagesLocally )
               {
                    String fullPath = savePageLocally(page, localFilePath, pageNum);
                    pageNum++;
                    System.out.println("Page with title '" + page.getTitleText() + "' saved to " + fullPath);
               }


               System.out.println("Test has completed successfully");
          }
          catch ( FailingHttpStatusCodeException e1 )
          {
               System.out.println( "FailingHttpStatusCodeException thrown:" + e1.getMessage() );
               e1.printStackTrace();

               if( savePagesLocally )
               {
                    String fullPath = savePageLocally(page, localFilePath, true, pageNum);
                    System.out.println(ERROR_PAGE + " saved to " + fullPath);
               }

          }
          catch ( MalformedURLException e1 )
          {
               System.out.println( "MalformedURLException thrown:" + e1.getMessage() );
               e1.printStackTrace();

               if( savePagesLocally )
               {
                    String fullPath = savePageLocally(page, localFilePath, true, pageNum);
                    System.out.println(ERROR_PAGE + " saved to " + fullPath);
               }

          }
          catch ( IOException e1 )
          {
               System.out.println( "IOException thrown:" + e1.getMessage() );
               e1.printStackTrace();

               if( savePagesLocally )
               {
                    String fullPath = savePageLocally(page, localFilePath, true, pageNum);
                    System.out.println(ERROR_PAGE + " saved to " + fullPath);
               }

          }
          catch( Exception e )
          {
               System.out.println( "General exception thrown:" + e.getMessage() );
               e.printStackTrace();

               if( savePagesLocally )
               {
                    String fullPath = savePageLocally(page, localFilePath, true, pageNum);
                    System.out.println(ERROR_PAGE + " saved to " + fullPath);
               }

          }
     }

     public static final String WINDOWS_OS = "Windows";
     public static final String ERROR_PAGE = "error_page";
     public static final String STANDARD_PAGE = "output";

     protected static String savePageLocally(HtmlPage page, String filePath, int pageNum)
     {
          return savePageLocally(page, filePath, false, pageNum);
     }

     protected static String savePageLocally(HtmlPage page, String filePath, boolean isErrorPage, int pageNum)
     {
          String fullFilePath = null;
          if( isErrorPage )
          { fullFilePath = filePath + ERROR_PAGE; }
          else
          { fullFilePath = filePath + STANDARD_PAGE + "_" + pageNum; }

          File saveFolder = new File(fullFilePath);

          // Overwrite the standard HtmlUnit .html page to add diagnostic info at the top
          File webPage = new File(fullFilePath + ".html");
          BufferedWriter writer = null;
          BufferedReader reader = null;
          try
          {
               // Save all the images and css files using the HtmlUnit API
               page.save(saveFolder);

               reader = new BufferedReader( new FileReader( webPage) );
               StringBuffer buffer = new StringBuffer();

               String line;
               while( (line = reader.readLine() ) != null )
               {
                    buffer.append( line );
                    buffer.append("\n");
               }

               writer = new BufferedWriter( new FileWriter( webPage ) );

               // Diagnostic info
               Throwable t = new Throwable();
               StackTraceElement[] trace= t.getStackTrace();

               // Get the line of code that called this method
               StackTraceElement callingElement = trace[trace.length-1];
               writer.write( "Java code: " + callingElement.toString() + "&nbsp;");

               if( isErrorPage )
               { writer.write( "<a href=" + STANDARD_PAGE + "_" + (pageNum-1) + ".html>Previous</a>" ); }
               else
               {
                    if( pageNum > 1)
                    { writer.write( "<a href=" + STANDARD_PAGE + "_" + (pageNum-1) + ".html>Previous</a>" ); }

                    writer.write( "&nbsp;<a href=" + STANDARD_PAGE + "_" + (pageNum+1) + ".html>Next</a>" );
                    writer.write( "&nbsp;<a href=" + ERROR_PAGE + ".html>Error page</a><br>");
               }

               // Main body of page as seen by HTMLUnit
               writer.write( buffer.toString() );
          }
          catch ( IOException e )
          {
               System.out.println( "IOException was thrown: " + e.getMessage() );
               e.printStackTrace();
          }
          finally
          {
               if( writer != null )
               {
                    try
                    {
                         writer.flush();
                         writer.close();
                    }
                    catch ( IOException e )
                    { }
               }
               if( reader != null )
               {
                    try
                    {
                         reader.close();
                    }
                    catch ( IOException e )
                    { }
               }
          }

          return fullFilePath + ".html";
     }
}
