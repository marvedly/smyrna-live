package se.smyrna.smyrnalive.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import se.smyrna.smyrnalive.model.ProgramSettings;
import se.smyrna.smyrnalive.model.UserInput;

public class BatchRunner {

	private BatchRunner() {}

	public static void startMXLightStreaming() {
		new Thread() {
			@Override
			public void run() {
				try {
					String mxLightlocation = ProgramSettings.getInstance().getMxlightLocation();
					ProgramSettings.getInstance().createMxlightSpecialFolders();
					Runtime.getRuntime().exec("cmd /c cd \"" + mxLightlocation + "\" & ECHO y | copy \"cfg\\cfgForStream.cda\" \"cfg\\config.cda\" & ECHO y | copy \"cfg\\strForStream.cda\" \"cfg\\streams.cda\" & mxlight.exe stream-profile='YouTube (1080p Stream)' stream=on gui-config=hide gui-stats=show");
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					Thread.currentThread().interrupt();
				}
			}
		}.start();
	}

	public static void stopMXLightStreaming() {
		try {
			String mxLightlocation = ProgramSettings.getInstance().getMxlightLocation();
			ProgramSettings.getInstance().createMxlightSpecialFolders();
			Runtime.getRuntime().exec("cmd /c cd \"" + mxLightlocation + "\" & mxlight.exe stream=off gui-config=hide gui-stats=show");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			Thread.currentThread().interrupt();
		}

	}


	public static void startMXLightRecording() {
		new Thread() {
			@Override
			public void run() {
				try {
					String mxLightlocation = ProgramSettings.getInstance().getMxlightLocation();
					ProgramSettings ps = ProgramSettings.getInstance();
					ps.createMxlightSpecialFolders();
					Runtime.getRuntime().exec("cmd /c cd \"" + mxLightlocation + "\" & ECHO y | copy \"cfg\\cfgForStream.cda\" \"cfg\\config.cda\" & ECHO y | copy \"cfg\\strForStream.cda\" \"cfg\\streams.cda\" & mxlight.exe record-to-file='" + ps.getMxlightRecordingFolderPath() + "\\" + UserInput.getInstance().getBroadCastTitle().replaceAll("(\\\\|/|:|\\*|\\?|\"|<|>|\\|)", "") + ".ts' record=on gui-config=hide gui-stats=show ");
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					Thread.currentThread().interrupt();
				}
			}
		}.start();
	}

	public static void stopMXLightRecording() {
		try {
			String mxLightlocation = ProgramSettings.getInstance().getMxlightLocation();
			ProgramSettings.getInstance().createMxlightSpecialFolders();
			Runtime.getRuntime().exec("cmd /c cd \"" + mxLightlocation + "\" & mxlight.exe record=off gui-config=hide gui-stats=show");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			Thread.currentThread().interrupt();
		}
	}

	public static void stopMXLight() {
		try {
			Runtime.getRuntime().exec("cmd /c taskkill /F /IM mxlight.exe");
			checkThatMXLightIsClosed();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected synchronized static void checkThatMXLightIsClosed() throws IOException {
		String line;
		String pidInfo = "";
		do {
			pidInfo = "";
			Process p = Runtime.getRuntime().exec(System.getenv("windir") +"\\system32\\"+"tasklist.exe");
			BufferedReader input =  new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = input.readLine()) != null) {
				pidInfo += line; 
			}
			input.close();

			if (pidInfo.toLowerCase().contains("mxlight.exe")) {
				Runtime.getRuntime().exec("cmd /c taskkill /F /IM mxlight.exe");
			}
		} while (pidInfo.toLowerCase().contains("mxlight.exe"));
	}
}
