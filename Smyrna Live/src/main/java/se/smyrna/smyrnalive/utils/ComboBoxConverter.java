package se.smyrna.smyrnalive.utils;

public class ComboBoxConverter {

	/**
	 * Returns values from Combobox with privacy statuses.
	 * <br>
	 * index = 0 => return "public"
	 * <br>
	 * index = 1 => return "unlisted"
	 * <br>
	 * index = 2 => return "private"
	 * <br>
	 * @param index an index 0, 1 or 2.
	 * @return a string as described above. Else null.
	 */
	public static String convertToString(int index) {
		switch (index) {
		case 0:
			return "public";
		case 1:
			return "unlisted";
		case 2:
			return "private";
		}
		return null;
	}

}
