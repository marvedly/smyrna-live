package se.smyrna.smyrnalive.utils;

import java.io.File;
import java.io.IOException;

import se.smyrna.smyrnalive.model.ProgramSettings;
import se.smyrna.smyrnalive.model.UserInput;

public class FileUtils {
	public static boolean isFilenameValid(String file, String shouldEndWith) {
		File f = new File(file);
		try {
			f.getCanonicalPath();
			if (file.endsWith(shouldEndWith) && file.length() > shouldEndWith.length()) {
				return true;
			} else {
				return false;
			}
		} catch (IOException e) {
			return false;
		}
	}
	
//	/**
//	 * No -> " <- are at the ends of the name. \ is replaced with \\
//	 * @param location
//	 * @return
//	 */
//	public static String toBatchFormat(String location) {
//		return location.replace("\\", "\\\\");
//	}
	
	
	public static void main(String args[]) throws Exception {
		ProgramSettings p = ProgramSettings.getInstance();
		System.out.println(System.getProperty("user.home")); // %userprofile%
		System.out.println(System.getProperty("user.home")); // %userprofile%
		System.out.println(p.getStreamsCDAPath());
		System.out.println(p.DEFAULT_MXLIGHT_PATH);
		System.out.println(p.DEFAULT_XML_FILE_PATH);
		System.out.println(p.DEFAULT_CONFIG_FOLDER_PATH);
		System.out.println(p.DEFAULT_DO_NOT_TOUCH_FILE_LOCATION);
		System.out.println(p.DEFAULT_PROGRAM_RESOURCE_LOCATION);
		System.out.println(p.getMxlightLocation());
		System.out.println();
		
		System.out.println(p.getMxlightRecordingFolderPath());
		
		String mxLightlocation = p.getMxlightLocation();
		System.out.println(mxLightlocation);
		System.out.println();
		System.out.println("BatchRunner commands:");
		// Start streaming
		System.out.println("cmd /c cd \"" + mxLightlocation + "\" & ECHO y | copy \"cfg\\cfgForStream.cda\" \"cfg\\config.cda\" & ECHO y | copy \"cfg\\strForStream.cda\" \"cfg\\streams.cda\" & mxlight.exe stream-profile='YouTube (1080p Stream)' stream=on gui-config=hide gui-stats=show");
		
		// Stop streaming
		System.out.println("cmd /c cd \"" + mxLightlocation + "\" & mxlight.exe stream=off gui-config=hide gui-stats=show");
		
		// Start record to file
		UserInput.getInstance().setBroadCastTitle("TestVideo");
		System.out.println("cmd /c cd \"" + p.getMxlightLocation() + "\" & ECHO y | copy \"cfg\\cfgForStream.cda\" \"cfg\\config.cda\" & ECHO y | copy \"cfg\\strForStream.cda\" \"cfg\\streams.cda\" & mxlight.exe record-to-file='" + p.getMxlightRecordingFolderPath() + "\\" + UserInput.getInstance().getBroadCastTitle().replaceAll("(\\\\|/|:|\\*|\\?|\"|<|>|\\|)", "") + ".ts' record=on gui-config=hide gui-stats=show ");
		
		// Stop record to file
		System.out.println("cmd /c cd \"" + mxLightlocation + "\" & mxlight.exe record=off gui-config=hide gui-stats=show");
		
		// Copy stream.cda file
		System.out.println("cmd /c cd \"" + mxLightlocation + "\" & ECHO y | copy \"cfg\\cfgForStream.cda\"");
		
		System.out.println();
		
//		System.out.println("To batch:");
//		System.out.println(toBatchFormat(p.getStreamsCDAPath()));
//		System.out.println(toBatchFormat(p.DEFAULT_MXLIGHT_PATH));
//		System.out.println(toBatchFormat(p.DEFAULT_XML_FILE_PATH));
//		System.out.println(toBatchFormat(p.DEFAULT_CONFIG_FOLDER_PATH));
//		System.out.println(toBatchFormat(p.DEFAULT_DO_NOT_TOUCH_FILE_LOCATION));
//		System.out.println(toBatchFormat(p.DEFAULT_PROGRAM_RESOURCE_LOCATION));
//		System.out.println(toBatchFormat(p.getMxlightLocation()));
		System.out.println();
		
	    // true
	    System.out.println(FileUtils.isFilenameValid("well.mp4",".mp4"));
	    System.out.println(FileUtils.isFilenameValid("well well.txt",""));
	    System.out.println(FileUtils.isFilenameValid("",""));

	    //false
	    System.out.println(FileUtils.isFilenameValid("test.T*T",""));
	    System.out.println(FileUtils.isFilenameValid("test|.TXT",""));
	    System.out.println(FileUtils.isFilenameValid("te?st.TXT",""));
	    System.out.println(FileUtils.isFilenameValid("con.TXT","")); // windows
	    System.out.println(FileUtils.isFilenameValid("prn.TXT","")); // windows
	    }
	
	
	
}
