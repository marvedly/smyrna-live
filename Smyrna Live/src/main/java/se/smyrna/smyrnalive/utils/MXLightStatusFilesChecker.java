package se.smyrna.smyrnalive.utils;

import java.io.File;

import se.smyrna.smyrnalive.model.ProgramSettings;

public class MXLightStatusFilesChecker {
	private static String streamingPath = ProgramSettings.getInstance().getMxlightLocation() + "/status/.streaming";
	private static String recordingPath = ProgramSettings.getInstance().getMxlightLocation() + "/status/.recording";
	
	private MXLightStatusFilesChecker() {}
	
	public static boolean checkIfStreamingFileExists() {
		return checkIfFileExists(streamingPath);
	}
	
	public static boolean checkIfRecordingFileExists() {
		return checkIfFileExists(recordingPath);
	}
	
	private static boolean checkIfFileExists(String path) {
		File file = new File(path);
		return file.exists();
	}
}
