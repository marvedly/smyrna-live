package se.smyrna.smyrnalive.utils;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Util class that can test internet connection.
 * @author Martin
 *
 */
public class InternetTester {
	private static boolean hasInternetConnection;
	private static String urlStatic;
	/**
	 * Tests Internet connection to a specific URL.
	 * @return true if connected, else false
	 */
	public static boolean testWebsite(String url) {
		urlStatic = url;
		hasInternetConnection = false;
		try {
			URL _url = new URL(urlStatic);
			final URLConnection conn = _url.openConnection();
			conn.connect();
			hasInternetConnection = true;
		} catch (IOException e) {
			hasInternetConnection = false;
		}
		return hasInternetConnection;
	}
}
