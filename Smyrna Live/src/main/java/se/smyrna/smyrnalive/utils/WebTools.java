package se.smyrna.smyrnalive.utils;

import java.io.IOException;
import java.net.MalformedURLException;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlCheckBoxInput;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;



public class WebTools {
	public static void main(String[] args) throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		WebClient webClient = new WebClient();
		HtmlPage page = null;
		java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
	    java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);
	    
		page = webClient.getPage("https://youtube.com/");
		System.out.println(page.getTitleText());
		
		System.out.println();
		System.out.println();
		
//		System.out.println(page.asText());
		
		System.out.println();
		System.out.println();
		
		
		webClient.closeAllWindows();
	}
	
	/**
	 * Must be signed in to YouTube in order to work!
	 * @param id the id of the YouTube live broadcast
	 * @param country the country code of time zone. (Type "SE" for Sweden).
	 * @param category the category (Type "27" for Education).
	 * @param allowComments set true if watchers are allowed to comment the live broadcast
	 * @param allowRatings set true if watchers are allowed to view ratings of the live broadcast
	 * @return true if success, else false
	 */
	public static boolean adjustYouTubeSettings(String id, String country, String category, boolean makePrivateWhenEventEnds, boolean allowComments, boolean allowRatings) {
		WebClient webClient = new WebClient();
		HtmlPage page;
		try {
			page = webClient.getPage("https://www.youtube.com/my_live_events?action_edit_live_event=1&event_id=" + id);
		} catch (FailingHttpStatusCodeException e) {
			e.printStackTrace();
			return false;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		// Change time zone
		
		HtmlForm form = page.getFormByName("mdeform");
		
		// Click button "Edit"
//		HtmlButton editButton = form.getButtonByName("yt-uix-button yt-uix-button-size-default yt-uix-button-link");
//		try {
//			page = editButton.click();
//		} catch (IOException e) {
//			e.printStackTrace();
//			return false;
//		}
		
		HtmlSelect selectCountry = form.getSelectByName("countries");
		page = selectCountry.setSelectedAttribute(country, true);
		
		HtmlSelect selectCategory = form.getSelectByName("category");
		page = selectCategory.setSelectedAttribute(category, true);

		HtmlCheckBoxInput checkBoxMakePrivate = page.getElementByName("make_private_on_complete");
		page = (HtmlPage) checkBoxMakePrivate.setChecked(makePrivateWhenEventEnds);
		
		HtmlCheckBoxInput checkBoxAllowComments = page.getElementByName("allow_comments");
		page = (HtmlPage) checkBoxAllowComments.setChecked(allowComments);
		
		HtmlCheckBoxInput checkBoxAllowRatings = page.getElementByName("allow_ratings");
		page = (HtmlPage) checkBoxAllowRatings.setChecked(allowRatings);
		
		HtmlSubmitInput saveChangesButton = form.getFirstByXPath("//button[@class='yt-uix-button yt-uix-button-size-default save-changes-button yt-uix-tooltip yt-uix-button-primary']");
		try {
			page = saveChangesButton.click();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		
		
		webClient.closeAllWindows();
		return true;
	}
	
}
