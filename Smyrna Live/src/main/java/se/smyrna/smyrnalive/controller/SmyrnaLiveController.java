package se.smyrna.smyrnalive.controller;


import java.io.IOException;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.naming.AuthenticationException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.wb.swt.SWTResourceManager;

import se.smyrna.smyrnalive.model.ProgramSettings;
import se.smyrna.smyrnalive.model.SmyrnaLive;
import se.smyrna.smyrnalive.model.TimeInstance;
import se.smyrna.smyrnalive.model.UserInput;
import se.smyrna.smyrnalive.model.events.DescriptableIEvent;
import se.smyrna.smyrnalive.model.events.FarsiService;
import se.smyrna.smyrnalive.model.events.IEvent;
import se.smyrna.smyrnalive.model.events.InternationalService;
import se.smyrna.smyrnalive.model.events.OtherEvent;
import se.smyrna.smyrnalive.model.events.SwedishService;
import se.smyrna.smyrnalive.utils.BatchRunner;
import se.smyrna.smyrnalive.utils.ComboBoxConverter;
import se.smyrna.smyrnalive.utils.DialogFactory;
import se.smyrna.smyrnalive.utils.InternetTester;
import se.smyrna.smyrnalive.utils.DoNotTouchFileCommunicator;
import se.smyrna.smyrnalive.utils.MXLightStatusFilesChecker;
import se.smyrna.smyrnalive.view.SmyrnaLiveView;
import se.smyrna.smyrnalive.view.StreamLengthPicker;
import se.smyrna.smyrnalive.youtube.CreateNewBroadcast;

public class SmyrnaLiveController implements SelectionListener, ModifyListener , PaintListener {
	private SmyrnaLive smyrnaLive;
	private SmyrnaLiveView smyrnaLiveView;
	private IEvent swedishService;
	private IEvent internationalService;
	private IEvent farsiService;
	private IEvent otherEvent;
	private CreateNewBroadcast broadcastCreator;
	private int gifImageNumber;
	private Thread animationThread;

	public SmyrnaLiveController(SmyrnaLive model, SmyrnaLiveView view) {
		this.smyrnaLive = model;
		this.smyrnaLiveView = view;
		createEvents();
		addListeners();
		markRadioButton(); // based on what is in preScheduled.smy
		setTags();
		resetTitleAsItWasWhenLastStreamStarted();
		boolean authNotOk = false;
		try {
			broadcastCreator = new CreateNewBroadcast();
		} catch (AuthenticationException e) {
			// display error message and System.exit(?);
			authNotOk = true;
			
			
		}
		if (smyrnaLive.getTitleAsItWasWhenStreamStarted() != null) {
			if (!broadcastCreator.setIdOnStartUpIfBroadcastExists(smyrnaLive.getTitleAsItWasWhenStreamStarted())) {
				// id wasn't present
				smyrnaLive.setTitleAsItWasWhenStreamStarted(null);
			}
		}
		smyrnaLive.setRecording(MXLightStatusFilesChecker.checkIfRecordingFileExists()); 
		smyrnaLive.setStreaming(MXLightStatusFilesChecker.checkIfStreamingFileExists());

		final int TIME = 100;
		Runnable timer = new Runnable() {

			@Override
			public void run() {
				smyrnaLive.setRecording(MXLightStatusFilesChecker.checkIfRecordingFileExists()); 
				smyrnaLive.setStreaming(MXLightStatusFilesChecker.checkIfStreamingFileExists());

				smyrnaLiveView.getShell().getDisplay().timerExec(TIME, this);
			}
		};
		smyrnaLiveView.getShell().getDisplay().timerExec(1, timer);
		if (authNotOk) {
			smyrnaLiveView.getShell().open();
			DialogFactory.authDeniedDialog(smyrnaLiveView.getShell());
			System.exit(1);
		}
	}

	private void createEvents() {
		swedishService = new SwedishService();
		internationalService = new InternationalService();
		farsiService = new FarsiService();
		otherEvent = new OtherEvent();
	}

	private void addListeners() {
		smyrnaLive.addPropertyChangeListener(smyrnaLiveView);
		smyrnaLiveView.getShell().addListener(SWT.Close, new Listener() {

			// Save settings when closing program
			@Override
			public void handleEvent(Event event) {
				ProgramSettings.getInstance().saveSettings();
				if (smyrnaLive.getTitleAsItWasWhenStreamStarted() != null) {
					DoNotTouchFileCommunicator.writeTitleAsItWasWhenStreamStartedToFile(smyrnaLive.getTitleAsItWasWhenStreamStarted());
				}
				if (animationThread != null && animationThread.isAlive()) {
					animationThread.interrupt();
				}
				event.doit = true;
			}
		});

		smyrnaLiveView.getBtnGoLive().addSelectionListener(this);
		smyrnaLiveView.getBtnStopRecording().addSelectionListener(this);
		smyrnaLiveView.getBtnStartRecording().addSelectionListener(this);
		smyrnaLiveView.getBtnStopStreaming().addSelectionListener(this);

		smyrnaLiveView.getRadioNormalSettings().addSelectionListener(this);
		smyrnaLiveView.getRadioAdvancedSettings().addSelectionListener(this);
		smyrnaLiveView.getRadioSwedishService().addSelectionListener(this);
		smyrnaLiveView.getRadioInternationalService().addSelectionListener(this);
		smyrnaLiveView.getRadioFarsiService().addSelectionListener(this);
		smyrnaLiveView.getRadioOther().addSelectionListener(this);
		smyrnaLiveView.getCbOverwriteExisting().addSelectionListener(this);

		smyrnaLiveView.getMntmSetXMLfolderPath().addSelectionListener(this);
		smyrnaLiveView.getMntmSetDefaultStreamLength().addSelectionListener(this);
		smyrnaLiveView.getMntmEnglish().addSelectionListener(this);
		smyrnaLiveView.getMntmSwedish().addSelectionListener(this);

		smyrnaLiveView.getDtStartDate().addSelectionListener(this);;
		smyrnaLiveView.getTextTopic().addModifyListener(this);
		smyrnaLiveView.getComboPreacher().addModifyListener(this);
		smyrnaLiveView.getComboSeries().addModifyListener(this);
		smyrnaLiveView.getComboPart().addModifyListener(this);
		smyrnaLiveView.getComboPartAll().addModifyListener(this);
		smyrnaLiveView.getTextTitlePreview().addModifyListener(this);
		smyrnaLiveView.getTextTags().addModifyListener(this);
		smyrnaLiveView.getTextDescription().addModifyListener(this);

		smyrnaLiveView.getCanvasLoading().addPaintListener(this);

	}

	private void markRadioButton() {
		ProgramSettings ps = ProgramSettings.getInstance();
		String type = ps.getServiceLanguage();
		if ("english".equals(type)) {
			smyrnaLive.setEvent(internationalService);
			smyrnaLiveView.getRadioInternationalService().setSelection(true);
			smyrnaLiveView.enableOther(false);
		} else if ("farsi".equals(type)) {
			smyrnaLive.setEvent(farsiService);
			smyrnaLiveView.getRadioFarsiService().setSelection(true);
			smyrnaLiveView.enableOther(false);
		} else if ("other".equals(type)) {
			smyrnaLive.setEvent(otherEvent);
			smyrnaLiveView.getRadioOther().setSelection(true);
			smyrnaLiveView.enableOther(true);
		} else { // Swedish is default if all fails (should not happen though)
			smyrnaLive.setEvent(swedishService);
			smyrnaLiveView.getRadioSwedishService().setSelection(true);
			smyrnaLiveView.enableOther(false);
		}
		buildTitle();
		buildDescription();
	}

	private void setTags() {
		ProgramSettings ps = ProgramSettings.getInstance();
		ps.readTags(new IEvent[]{swedishService, internationalService, farsiService, otherEvent});
		smyrnaLiveView.getTextTags().setText(smyrnaLive.getEvent().getTags());
	}

	private void resetTitleAsItWasWhenLastStreamStarted() {
		smyrnaLive.setTitleAsItWasWhenStreamStarted(DoNotTouchFileCommunicator.getTitleAsItWasWhenStreamStartedToFile());
		decideNameForGoLiveButton();
	}

	private void openFolderWindow() {
		ProgramSettings ps = ProgramSettings.getInstance();
		DirectoryDialog  dd = new DirectoryDialog(smyrnaLiveView.getShell());
		dd.setFilterPath(ps.getxmlFolderLocation());
		dd.setMessage(ResourceBundle.getBundle(smyrnaLiveView.BUNDLE_NAME).getString("SmyrnaLiveView_folderMessage"));
		String result = dd.open();
		if (result != null) {
			ps.setxmlFolderLocation(result);
		}
	}

	private void buildTitle() {
		if (!(smyrnaLive.getEvent() instanceof OtherEvent)) { // if any service is marked (not "Other")
			StringBuilder broadcastTitle = new StringBuilder(50);
			broadcastTitle.append(smyrnaLiveView.getTimeYYMMDD());
			broadcastTitle.append(" ");
			broadcastTitle.append(smyrnaLive.getEvent().getName());
			broadcastTitle.append(" - ");
			broadcastTitle.append(smyrnaLiveView.getTextTopic().getText().trim());
			if (!broadcastTitle.substring(broadcastTitle.length()-1, broadcastTitle.length()).equals(" ")) { // if last char isn't an empty space
				broadcastTitle.append(", ");
			}
			broadcastTitle.append(smyrnaLiveView.getComboPreacher().getText().trim());
			smyrnaLive.setTitle(broadcastTitle.toString());
		} else {
			smyrnaLiveView.getTextTitlePreview().setText(smyrnaLive.getOtherTitle()); // if "Other" is marked
		}
	}

	private void buildDescription() {
//			String description = ProgramSettings.getInstance().getLastReadDescription();
//
//			description = StringUtils.replaceIfNotEmpty("[PREACHER]", smyrnaLiveView.getComboPreacher().getText());
//			description = StringUtils.replaceIfNotEmpty("[TOPIC]", smyrnaLiveView.getTextTopic().getText());
//			description = StringUtils.replaceIfNotEmpty("[SERVICE_NBR]", smyrnaLiveView.getTextTopic().getText());
//			description = StringUtils.replaceIfNotEmpty("[SERVICE_NBR_TOTAL]", smyrnaLiveView.getTextTopic().getText());
//			description = StringUtils.replaceIfNotEmpty("[SERIES]", smyrnaLiveView.getTextTopic().getText());
//			description = StringUtils.replaceIfNotEmpty("[YEAR]", smyrnaLiveView.getTextTopic().getText());

			
		ProgramSettings ps = ProgramSettings.getInstance();
		StringBuilder sb = new StringBuilder();

		if (!(smyrnaLive.getEvent() instanceof OtherEvent)) { // if any service is marked (not "Other")
			DescriptableIEvent e = (DescriptableIEvent) smyrnaLive.getEvent();
			sb.append(e.getDescription_sermon());
			sb.append(" ");
			sb.append(smyrnaLiveView.getComboPreacher().getText());
			sb.append(" - ");
			sb.append(smyrnaLiveView.getTextTopic().getText());
			if (smyrnaLiveView.getComboSeries().getText().length() > 0
					&& (smyrnaLiveView.getComboPart().getText().length() > 0
					|| smyrnaLiveView.getComboPartAll().getText().length() > 0)) {
				sb.append("\n\n");
				sb.append(e.getDescription_nbr());
				sb.append(" ");
				if (smyrnaLiveView.getComboPart().getText().length() > 0) {
					sb.append(smyrnaLiveView.getComboPart().getText());
				} else {
					sb.append("X");
				}
				sb.append(" ");
				sb.append(e.getDescription_of());
				sb.append(" ");
				if (smyrnaLiveView.getComboPartAll().getText().length() > 0) {
					sb.append(smyrnaLiveView.getComboPartAll().getText());
				} else {
					sb.append("Y");
				}
				sb.append(" ");
				sb.append(e.getDescription_in_series());
				sb.append(" ");
				if (smyrnaLiveView.getComboSeries().getText().length() > 0) {
					sb.append(smyrnaLiveView.getComboSeries().getText());
				} else {
					sb.append("QQQQ");
				}
			} else if (smyrnaLiveView.getComboSeries().getText().length() > 0
					&& smyrnaLiveView.getComboPart().getText().length() == 0
					&& smyrnaLiveView.getComboPartAll().getText().length() == 0) {
				sb.append("\n\n");
				sb.append(e.getDescription_in_series().substring(0,1).toUpperCase());
				sb.append(e.getDescription_in_series().substring(1, e.getDescription_in_series().length()));
				sb.append(" ");
				sb.append(smyrnaLiveView.getComboSeries().getText());
			}
			sb.append("\n\n");
			sb.append(e.getDescription_Smyrna());
			sb.append("\n");
			sb.append(smyrnaLiveView.getDtStartDate().getYear());
			sb.append("\n\n");
			sb.append(ps.readDescription(ps.getServiceLanguage() + " suffix")); // may be license and so on...
			smyrnaLiveView.getTextDescription().setText(sb.toString());
		} else {
			sb.append("\n\n");
			sb.append(ps.readDescription(ps.getServiceLanguage() + " suffix"));
			smyrnaLiveView.getTextDescription().setText(sb.toString()); // if "Other" is marked
		}
	}

	protected void startAnimation() {
		gifImageNumber = 0;
		animationThread = new Thread() {

			@Override
			public void run() {
				while (smyrnaLive.isShowingLoadingIcon()) {					
					try {
						int ms = smyrnaLiveView.getLoader().data[gifImageNumber].delayTime * 10;
						if (ms < 20) ms += 30;
						if (ms < 30) ms += 10;
						Thread.sleep(ms);
					} catch (InterruptedException e) {
						if (animationThread.isAlive()) {
							Thread.currentThread().interrupt();
						}
					}
					try {
						smyrnaLiveView.getShell().getDisplay().asyncExec(new Runnable() {

							@Override
							public void run() {
								Image frameImage = null;
								gifImageNumber = gifImageNumber == smyrnaLiveView.getLoader().data.length-1 ? 0 : gifImageNumber+1;
								ImageData nextFrameData = smyrnaLiveView.getLoader().data[gifImageNumber];
								frameImage = new Image(smyrnaLiveView.getCanvasLoading().getDisplay(),nextFrameData);
								smyrnaLiveView.getGc().drawImage(frameImage, nextFrameData.x, nextFrameData.y);
								frameImage.dispose();
								smyrnaLiveView.getCanvasLoading().redraw();
							}
						});
					} catch (SWTException e) {}
				}
			}
		};
		animationThread.start();
	}

	private void twinkleTextTitlePreview() {
		final RGB PINK = new RGB(255, 176, 176);
		final RGB WHITE = new RGB(255, 255, 255);
		final int TWINKLE_TIME = 150;
		smyrnaLiveView.getTextTitlePreview().setBackground(SWTResourceManager.getColor(PINK));
		smyrnaLiveView.getShell().getDisplay().timerExec(TWINKLE_TIME, new Runnable() {

			@Override
			public void run() {
				smyrnaLiveView.getTextTitlePreview().setBackground(SWTResourceManager.getColor(WHITE));
				smyrnaLiveView.getShell().getDisplay().timerExec(TWINKLE_TIME, new Runnable() {

					@Override
					public void run() {
						smyrnaLiveView.getTextTitlePreview().setBackground(SWTResourceManager.getColor(PINK));
						smyrnaLiveView.getShell().getDisplay().timerExec(TWINKLE_TIME, new Runnable() {

							@Override
							public void run() {
								smyrnaLiveView.getTextTitlePreview().setBackground(SWTResourceManager.getColor(WHITE));
								smyrnaLiveView.getShell().getDisplay().timerExec(TWINKLE_TIME, new Runnable() {

									@Override
									public void run() {
										smyrnaLiveView.getTextTitlePreview().setBackground(SWTResourceManager.getColor(PINK));
										smyrnaLiveView.getShell().getDisplay().timerExec(TWINKLE_TIME, new Runnable() {

											@Override
											public void run() {
												smyrnaLiveView.getTextTitlePreview().setBackground(SWTResourceManager.getColor(WHITE));
												smyrnaLiveView.getShell().getDisplay().timerExec(TWINKLE_TIME, new Runnable() {

													@Override
													public void run() {
														smyrnaLiveView.getTextTitlePreview().setBackground(SWTResourceManager.getColor(PINK));
														smyrnaLiveView.getShell().getDisplay().timerExec(TWINKLE_TIME, new Runnable() {

															@Override
															public void run() {
																smyrnaLiveView.getTextTitlePreview().setBackground(SWTResourceManager.getColor(WHITE));		
															}
														});
													}
												});
											}
										});
									}
								});
							}
						});
					}
				});
			}
		});		
	}

	public void updateComboPartAllIfPossible() {
		ProgramSettings ps = ProgramSettings.getInstance();
		for (int i = 0; i < ps.getSeries().length; i++) {
			if (ps.getSeries()[i].equals(smyrnaLiveView.getComboSeries().getText())) {
				if (ps.getSeriesNbr()[i] != null) {
					smyrnaLiveView.getComboPartAll().setText(String.valueOf(ps.getSeriesNbr()[i]));
				}
				return;
			}
		}
	}

	private void determineIfTitleHasChanged() {
		if (smyrnaLive.getTitleAsItWasWhenStreamStarted() == null) {
			smyrnaLive.setTitleChanged(false);
		} else {
			if(smyrnaLiveView.getTextTitlePreview().getText().trim().equals(smyrnaLive.getTitleAsItWasWhenStreamStarted().trim())) {
				smyrnaLive.setTitleChanged(false);
			} else {
				smyrnaLive.setTitleChanged(true);
			}
		}
	}

	private void decideNameForGoLiveButton() {
		// vi ska ha �ppna kontrollrum om overwrite �r urkryssad och n�r titeln inte har �ndrats fr�n vad den var n�r streamen startades,
		// allts�: om titeln.trim == getTitleAsItWaswhenstreamstarted.trim  och cb falsk s ska �ppna kontrollrum visas. Annars det andra.

		//vi ska ha Skapa live-evenemang annars 
		if (!smyrnaLiveView.getCbOverwriteExisting().getSelection()
				&& smyrnaLive.getTitleAsItWasWhenStreamStarted() != null
				&& smyrnaLive.getTitleAsItWasWhenStreamStarted().trim().equals(smyrnaLiveView.getTextTitlePreview().getText().trim())
				&& !smyrnaLive.isTitleChanged()) {
			smyrnaLiveView.getBtnGoLive().setText(ResourceBundle.getBundle(smyrnaLiveView.BUNDLE_NAME).getString("SmyrnaLiveView_btnGoLive_text2"));
			smyrnaLive.setEventCreated(true);
			smyrnaLiveView.getBtnGoLive().setToolTipText(ResourceBundle.getBundle(smyrnaLiveView.BUNDLE_NAME).getString("SmyrnaLiveView_btnGoLive_toolTipText2"));
			smyrnaLive.setEventCreated(true);
		} else {
			smyrnaLiveView.getBtnGoLive().setText(ResourceBundle.getBundle(smyrnaLiveView.BUNDLE_NAME).getString("SmyrnaLiveView_btnGoLive_text"));
			smyrnaLive.setEventCreated(false);
			smyrnaLiveView.getBtnGoLive().setToolTipText(ResourceBundle.getBundle(smyrnaLiveView.BUNDLE_NAME).getString("SmyrnaLiveView_btnGoLive_toolTipText"));
			smyrnaLive.setEventCreated(false);
		}
	}

	private void fetchUserInput() {
		UserInput ui = UserInput.getInstance();

		ui.setBroadCastTitle(smyrnaLive.getTitle());
		TimeZone tz = TimeZone.getDefault();
		
		boolean inDs = tz.inDaylightTime(new Date());
		ui.setStartTime(String.format("%04d-%02d-%02dT%02d:%02d:00+0" + (inDs?"2":"1") + ":00", smyrnaLiveView.getDtStartDate().getYear(),
				smyrnaLiveView.getDtStartDate().getMonth()+1, smyrnaLiveView.getDtStartDate().getDay(),
				smyrnaLiveView.getDtStartTime().getHours(), smyrnaLiveView.getDtStartTime().getMinutes()));
		ui.setEndTime(String.format("%04d-%02d-%02dT%02d:%02d:00+0" + (inDs?"2":"1") + ":00", smyrnaLiveView.getDtEndDate().getYear(),
				smyrnaLiveView.getDtEndDate().getMonth()+1, smyrnaLiveView.getDtEndDate().getDay(),
				smyrnaLiveView.getDtEndTime().getHours(), smyrnaLiveView.getDtEndTime().getMinutes()));
		System.out.println(ui.getStartTime());
		ui.setDescription(smyrnaLiveView.getTextDescription().getText());


		ui.setPrivacyStatus(ComboBoxConverter.convertToString(smyrnaLiveView.getComboPrivacyStatus().getSelectionIndex()));

		ui.setOverwriting(smyrnaLiveView.getCbOverwriteExisting().getSelection());
		ui.setTags(smyrnaLiveView.getTextTags().getText().split(","));
		ui.setLocation("Smyrnaf�rsamlingen i G�teborg");
		ui.setType(smyrnaLive.getEvent());
	}

	@Override
	public void widgetSelected(SelectionEvent e) {
		ProgramSettings ps = ProgramSettings.getInstance();
		if (e.getSource() instanceof Button) {
			Button button = (Button)e.getSource();
			if ("compositeProfiles".equals(button.getParent().getData())) {
				if ("radioOther".equals(button.getData())) {
					ps.setServiceLanguage("other");
					smyrnaLive.setEvent(otherEvent);
					smyrnaLiveView.enableOther(true);
					smyrnaLiveView.enableSettings(false);
					smyrnaLiveView.enableAdvancedSettings(true);
					setRecordingTitle();
				} else {
					smyrnaLiveView.enableSettings(true);
					if ("radioSwedishService".equals(button.getData())) {
						ps.setServiceLanguage("swedish");
						smyrnaLive.setEvent(swedishService);
					} else if ("radioInternationalService".equals(button.getData())) {
						ps.setServiceLanguage("english");
						smyrnaLive.setEvent(internationalService);
					} else if ("radioFarsiService".equals(button.getData())) {
						ps.setServiceLanguage("farsi");
						smyrnaLive.setEvent(farsiService);
					}
					smyrnaLiveView.enableOther(false);
					smyrnaLiveView.enableAdvancedSettings(smyrnaLiveView.getRadioAdvancedSettings().getSelection());
				}
				smyrnaLiveView.getTextTags().setText(smyrnaLive.getEvent().getTags());
				buildTitle();
				buildDescription();
			} else if ("cbOverwriteExisting".equals(button.getData())) {
				decideNameForGoLiveButton();
			} else if ("btnGoLive".equals((String)button.getData())) {
				if (smyrnaLiveView.getTextTitlePreview().getText().length() > 0) {
					if (InternetTester.testWebsite("http://www.youtube.com/")) { // if connected to Internet
						if (smyrnaLiveView.getBtnGoLive().getText().equals(ResourceBundle.getBundle(smyrnaLiveView.BUNDLE_NAME).getString("SmyrnaLiveView_btnGoLive_text"))) { // Create live event

							boolean goOn = true;
							if (ComboBoxConverter.convertToString(smyrnaLiveView.getComboPrivacyStatus().getSelectionIndex()).equals("public")) {
								// kolla om man VERKLIGEN vill skapa ett OFFENTLIGT evenemang. Detta kan skapa notiser hos f�ljare.
								goOn = DialogFactory.createGoPublicDialog(smyrnaLiveView.getShell());
							}
							if (goOn) { // one is sure that one wants to create an official event
								goOn = true;
								if ((smyrnaLive.isRecording() || smyrnaLive.isStreaming())) {
									goOn = DialogFactory.createStopMXLightDialog(smyrnaLiveView.getShell(), smyrnaLive.isRecording(), smyrnaLive.isStreaming());
								}
								if (goOn) { // one wants to stop current streaming and recording and restart MXLight
									if (smyrnaLiveView.getRadioOther().getSelection()) {
										smyrnaLive.setTitle(smyrnaLive.getOtherTitle());
									}

									smyrnaLive.setTitleAsItWasWhenStreamStarted(smyrnaLive.getTitle());

									smyrnaLiveView.getBtnGoLive().setText("");
									smyrnaLiveView.getBtnGoLive().setToolTipText(ResourceBundle.getBundle(smyrnaLiveView.BUNDLE_NAME).getString("SmyrnaLiveView_btnGoLive_toolTipText2"));
									smyrnaLive.setEventCreated(true);

									fetchUserInput();

									smyrnaLiveView.getCbOverwriteExisting().setSelection(false);

									smyrnaLiveView.getCanvasLoading().setVisible(true);
									smyrnaLive.setShowingLoadingIcon(true);
									smyrnaLiveView.getBtnGoLive().setEnabled(false);
									startAnimation();
									
									new Thread() {
										@Override
										public void run() {
											int counter = 0;
											boolean errorAtGoogle = false;
											do { // tries 3 times max
												errorAtGoogle = broadcastCreator.createBroadCast();
												if (errorAtGoogle) {
													UserInput.getInstance().setOverwriting(true);
													counter++;
												}
											} while (errorAtGoogle && counter < 3);
											if (errorAtGoogle) {
												// Something went wrong at Google's servers. Try again...
												DialogFactory.internalErrorAtGoogleDialog(smyrnaLiveView.getShell());
												smyrnaLiveView.getShell().getDisplay().asyncExec(new Runnable() {

													@Override
													public void run() {
														smyrnaLiveView.getCbOverwriteExisting().setSelection(true);
													}
													
												});
											}
											try {
												smyrnaLiveView.getShell().getDisplay().asyncExec(new Runnable() {

													@Override
													public void run() {
														smyrnaLiveView.getCanvasLoading().setVisible(false);
														animationThread.interrupt();
														smyrnaLive.setShowingLoadingIcon(false);
														smyrnaLiveView.getBtnGoLive().setText(ResourceBundle.getBundle(smyrnaLiveView.BUNDLE_NAME).getString("SmyrnaLiveView_btnGoLive_text2"));
														smyrnaLive.setEventCreated(true);
														smyrnaLiveView.getBtnGoLive().setEnabled(true);
													}
												});
											} catch (SWTException e) {}
											Thread.currentThread().interrupt();
										}
									}.start();
								}
							}
						} else { // Open control room
							new Thread() {
								@Override
								public void run() {
									try {
										broadcastCreator.openControlRoom();
									} catch (IOException e) {
										e.printStackTrace();
									} finally {
										Thread.currentThread().interrupt();
									}
								}
							}.start();
						}
					} else { // if not connected to Internet
						if (!smyrnaLiveView.getLblIncorrect().getVisible()) {
							smyrnaLiveView.getLblIncorrect().setVisible(true);
							smyrnaLiveView.getLblIncorrect().getDisplay().timerExec(2000, new Runnable() { // delay 2 seconds

								@Override
								public void run() {
									smyrnaLiveView.getLblIncorrect().setVisible(false);
								}

							});
						}
					}
				} else { //length of title == 0
					twinkleTextTitlePreview();
				}
			} else if ("btnStopStreaming".equals(button.getData())) {
				if (DialogFactory.stopStreamingDialog(smyrnaLiveView.getShell())) {
//					boolean wentFine = broadcastCreator.transitionToComplete();
//					if (!wentFine) {
//						wentFine = DialogFactory.streamIsStillActiveDialog(smyrnaLiveView.getShell());
//					}
//					// TODO stop/end YouTube event first (Transition)
//					if (wentFine) {
						BatchRunner.stopMXLightStreaming();
//					}
				}
			} else if ("btnStopRecording".equals(button.getData())) {
				BatchRunner.stopMXLightRecording();
			} else if ("btnStartRecording".equals(button.getData())) {
				BatchRunner.startMXLightRecording();
			} else if ("radioNormal".equals((String)button.getData())) {
				smyrnaLiveView.enableAdvancedSettings(false);
			} else if ("radioAdvanced".equals((String)button.getData())) {
				smyrnaLiveView.enableAdvancedSettings(true);
			} 
		} else if (e.getSource() instanceof MenuItem) {
			MenuItem menuItem = (MenuItem)e.getSource();
			if ("xmlPath".equals((String)menuItem.getData())) {
				openFolderWindow();
			} else if ("streamLength".equals((String)menuItem.getData())) {
				smyrnaLiveView.getShell().setEnabled(false);
				StreamLengthPicker slp = new StreamLengthPicker(smyrnaLiveView.getShell(), SWT.OK | SWT.CANCEL);
				ps = ProgramSettings.getInstance();

				TimeInstance result = slp.open();
				if (result != null) {
					ps.setDefaultStreamingLength(result);
					smyrnaLiveView.updateEndTimeToDefault();
				}

			} else if ("english".equals((String)menuItem.getData())) {
				Locale.setDefault(new Locale("en"));
				smyrnaLiveView.setTextToButtons();
			} else if ("swedish".equals((String)menuItem.getData())) {
				Locale.setDefault(new Locale("sv", "SE"));
				smyrnaLiveView.setTextToButtons();
			}
		}

		else if (e.getSource() instanceof Widget) {
			Widget widget = (Widget)e.getSource();
			if ("dtStartDate".equals(widget.getData())) {
				buildTitle();
			}
		}
	}

	@Override
	public void widgetDefaultSelected(SelectionEvent e) {}

	@Override
	public void paintControl(PaintEvent e) {
		e.gc.drawImage(smyrnaLiveView.getLoadingImage(), 0, 0);
	}

	@Override
	public void modifyText(ModifyEvent e) {
		if (e.getSource() instanceof Widget) {
			Widget widget = (Widget)e.getSource();
			if ("textPreacher".equals(widget.getData())
					|| "textTopic".equals(widget.getData())) {
				buildTitle();
				buildDescription();
			} else if ("textSeries".equals(widget.getData())
					|| "textPart".equals(widget.getData())
					|| "textPartAll".equals(widget.getData())) {
				if ("textSeries".equals(widget.getData())) {
					updateComboPartAllIfPossible();
				}
				buildDescription();
			} else if ("textTitlePreview".equals(widget.getData())) {
				setRecordingTitle();
				if (!smyrnaLiveView.getComboPreacher().isEnabled()) { // To fix so that otherTitle updates correctly
					smyrnaLive.setOtherTitle(smyrnaLiveView.getTextTitlePreview().getText());
				}
				determineIfTitleHasChanged();
				decideNameForGoLiveButton();

			} else if ("textDescription".equals(widget.getData())) {
				if (!smyrnaLiveView.getComboPreacher().isEnabled()) {
					smyrnaLive.setOtherDescription(smyrnaLiveView.getTextDescription().getText());
				}

			} else if ("textTags".equals(widget.getData())) {
				smyrnaLive.getEvent().setTags(smyrnaLiveView.getTextTags().getText());
			}
		}
	}

	private void setRecordingTitle() {
		UserInput ui = UserInput.getInstance();
		String title = smyrnaLiveView.getTextTitlePreview().getText();
		if (title != null && title.length() > 0) {
			ui.setBroadCastTitle(title);
		} else {
			ui.setBroadCastTitle("recording " + smyrnaLiveView.getTimeYYMMDD()); //default record title
		}		
	}

}
