package se.smyrna.smyrnalive.controller;

import se.smyrna.smyrnalive.model.SmyrnaLive;
import se.smyrna.smyrnalive.view.SmyrnaLiveView;

public class Main {

	public static void main(String[] args) {
		SmyrnaLive mainModel = new SmyrnaLive();		
		SmyrnaLiveView window = new SmyrnaLiveView(mainModel);
		new SmyrnaLiveController(mainModel, window);
		window.open();
	}
}
