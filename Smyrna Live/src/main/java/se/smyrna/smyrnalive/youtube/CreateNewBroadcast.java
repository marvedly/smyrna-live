package se.smyrna.smyrnalive.youtube;

import java.awt.Desktop;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.naming.AuthenticationException;

import se.smyrna.smyrnalive.Auth;
import se.smyrna.smyrnalive.model.MXLIGHTFileGenerator;
import se.smyrna.smyrnalive.model.ProgramSettings;
import se.smyrna.smyrnalive.model.UserInput;
import se.smyrna.smyrnalive.utils.BatchRunner;
import se.smyrna.smyrnalive.utils.MXLightStatusFilesChecker;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.util.DateTime;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTube.Thumbnails.Set;
import com.google.api.services.youtube.model.CdnSettings;
import com.google.api.services.youtube.model.GeoPoint;
import com.google.api.services.youtube.model.LiveBroadcast;
import com.google.api.services.youtube.model.LiveBroadcastListResponse;
import com.google.api.services.youtube.model.LiveBroadcastSnippet;
import com.google.api.services.youtube.model.LiveBroadcastStatus;
import com.google.api.services.youtube.model.LiveStream;
import com.google.api.services.youtube.model.LiveStreamListResponse;
import com.google.api.services.youtube.model.LiveStreamSnippet;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoListResponse;
import com.google.api.services.youtube.model.VideoRecordingDetails;
import com.google.api.services.youtube.model.VideoSnippet;
import com.google.common.collect.Lists;

public class CreateNewBroadcast {
	private YouTube youtube;
	private String id;
	private String streamId;
	private String streamName;

	public CreateNewBroadcast() throws AuthenticationException {
		List<String> scopes = Lists.newArrayList("https://www.googleapis.com/auth/youtube");
		try {
			Credential credential = Auth.authorize(scopes, "SmyrnaLiveOAuthCache");
			youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, credential)
			.setApplicationName("Smyrna Live").build();
		} catch (IOException e) {
			// Authentication failed.
			throw new AuthenticationException("Authentication failed");
		}
	}

	/**
	 * Create new broadcast on YouTube.
	 * @return true if no Google exception, else false
	 */
	public boolean createBroadCast() {
		try {
			UserInput ui = UserInput.getInstance();
			String title = ui.getBroadCastTitle();
			LiveBroadcastSnippet broadcastSnippet = new LiveBroadcastSnippet();
			broadcastSnippet.setTitle(title);
			broadcastSnippet.setPublishedAt(new DateTime(System.currentTimeMillis(),60));
			broadcastSnippet.setScheduledStartTime(new DateTime(ui.getStartTime()));
			broadcastSnippet.setScheduledEndTime(new DateTime(ui.getEndTime()));
			broadcastSnippet.setDescription(ui.getDescription());

			LiveBroadcastStatus status = new LiveBroadcastStatus();
			status.setPrivacyStatus(ui.getPrivacyStatus());

			//			LiveBroadcastContentDetails contentDetails = new LiveBroadcastContentDetails();
			//			contentDetails.setRecordFromStart(true);
			//			contentDetails.setEnableDvr(true);
			//			contentDetails.setEnableEmbed(true);

			LiveBroadcast broadcast = getExistingBroadcast();
			LiveBroadcast returnedBroadcast;

			if (broadcast == null) {
				broadcast = new LiveBroadcast();
				broadcast.setKind("youtube#liveBroadcast");
				broadcast.setSnippet(broadcastSnippet);
				broadcast.setStatus(status);
				//				broadcast.setContentDetails(contentDetails);

				YouTube.LiveBroadcasts.Insert liveBroadcastInsert =
						youtube.liveBroadcasts().insert("snippet,status,contentDetails", broadcast);

				returnedBroadcast = 
						liveBroadcastInsert.execute();
			} else {
				returnedBroadcast = broadcast;
			}
			id = returnedBroadcast.getId();

			YouTube.LiveStreams.List livestreamRequest = youtube.liveStreams().list("id,snippet");

			livestreamRequest.setMine(true);

			//			LiveStreamListResponse returnedListResponse = livestreamRequest.execute();

			//			List<LiveStream> returnedList = returnedListResponse.getItems();

			LiveStream returnedStream = null;
			//			for (LiveStream liveStream : returnedList) {
			//				if (liveStream.getSnippet().getTitle().equals(ui.streamTitle)) {
			//					returnedStream = liveStream;
			//					break;
			//				}
			//			}
			CdnSettings cdnSettings = new CdnSettings();
			cdnSettings.setFormat("1080p");
			cdnSettings.setIngestionType("rtmp");

			//			if (returnedStream == null) { // if 'Streaming Smyrna' don't exist, create it



			LiveStreamSnippet streamSnippet = new LiveStreamSnippet();
			streamSnippet.setTitle(ui.streamTitle);

			LiveStream stream = new LiveStream();
			stream.setKind("youtube#liveStream");
			stream.setSnippet(streamSnippet);
			stream.setCdn(cdnSettings);

			YouTube.LiveStreams.Insert liveStreamInsert =
					youtube.liveStreams().insert("snippet,cdn", stream);

			returnedStream = liveStreamInsert.execute();
			streamId = returnedStream.getId();



			//			} else { // update if it exists
			//				returnedStream.setCdn(cdnSettings);
			//				YouTube.LiveStreams.Update liveStreamUpdate =
			//						youtube.liveStreams().update("snippet,cdn", returnedStream);
			//				returnedStream = liveStreamUpdate.execute();
			//			}

			streamName = returnedStream.getCdn().getIngestionInfo().getStreamName();

			// export settings to file that MXLIGHT will use
			
			try {
				ProgramSettings ps = ProgramSettings.getInstance();
				String folderPath = ps.getxmlFolderLocation();
				File folder = new File(folderPath);
				if (!folder.exists()) {
					folder.mkdirs();
				}
				FileOutputStream fileOutputStream =
						new FileOutputStream(new File(MXLIGHTFileGenerator.getInstance().generateFMLE_XMLFileName(folderPath, id)));

				MXLIGHTFileGenerator mxlightFileGenerator = MXLIGHTFileGenerator.getInstance();
				mxlightFileGenerator.generateFMLE_XMLFile(fileOutputStream, streamName);

				fileOutputStream.close();
			
				String filePath = ps.getStreamsCDAPath();
				File streamConfigFile = new File(filePath);
				if (!streamConfigFile.getParentFile().exists()) {
					streamConfigFile.getParentFile().mkdirs();
				}
				if (!streamConfigFile.exists()) {
					String mxLightlocation = ps.getMxlightLocation();
					ProgramSettings.getInstance().createMxlightSpecialFolders();
					Runtime.getRuntime().exec("cmd /c cd \"" + mxLightlocation + "\" & ECHO y | copy \"cfg\\cfgForStream.cda\"");
				}
				FileOutputStream fileOutputStream2 =
						new FileOutputStream(streamConfigFile);

				mxlightFileGenerator.generateStreamsCDAFile(fileOutputStream2, streamName);

				fileOutputStream2.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			YouTube.LiveBroadcasts.Bind liveBroadcastBind =
					youtube.liveBroadcasts().bind(returnedBroadcast.getId(), "id,contentDetails");

			liveBroadcastBind.setStreamId(returnedStream.getId());

			returnedBroadcast = liveBroadcastBind.execute();
			
			if (MXLightStatusFilesChecker.checkIfRecordingFileExists()) {
				BatchRunner.stopMXLightRecording();
				while (MXLightStatusFilesChecker.checkIfRecordingFileExists());
			}
			
			if (MXLightStatusFilesChecker.checkIfStreamingFileExists()) {
				BatchRunner.stopMXLightStreaming();
				while (MXLightStatusFilesChecker.checkIfStreamingFileExists());
			}
			BatchRunner.stopMXLight();
			BatchRunner.startMXLightStreaming();
			setTagsAndLocation();
			uploadThumbnail();
//			WebTools.adjustYouTubeSettings(id, "SE", "27", true, false, false);
			openControlRoom();
		} catch (GoogleJsonResponseException e) {
			System.err.println("GoogleJsonResponseException code: " + e.getDetails().getCode() + " : " +
					e.getDetails().getMessage());
			e.printStackTrace();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public void openControlRoom() throws IOException {
		// Start web browser
		try {
			Desktop.getDesktop().browse(new URI("https://www.youtube.com/live_event_analytics?v=" + id));
		} catch (URISyntaxException e) {
			System.out.println("Error when trying to open web browser");
			e.printStackTrace();
		}		
	}

	private void setTagsAndLocation() throws IOException {
		UserInput ui = UserInput.getInstance();
		YouTube.Videos.List listVideosRequest = youtube.videos().list("snippet").setId(id);
		VideoListResponse listResponse = listVideosRequest.execute();
		List<Video> videoList = listResponse.getItems();
		if (videoList.isEmpty()) {
			return; //should not happen
		}
		Video video = videoList.get(0);
		VideoSnippet snippet = video.getSnippet();
		snippet.setTags(ui.getTags());

		VideoRecordingDetails videoRecordingDetails = new VideoRecordingDetails();
		videoRecordingDetails.setRecordingDate(new DateTime(ui.getStartTime()));
		videoRecordingDetails.setLocationDescription(ui.getLocation());
		GeoPoint geoPoint = new GeoPoint();
		geoPoint.setLatitude(57.6992632);
		geoPoint.setLongitude(11.9632819);
		videoRecordingDetails.setLocation(geoPoint);
		video = video.setRecordingDetails(videoRecordingDetails);

		YouTube.Videos.Update updateVideosRequest = youtube.videos().update("id,snippet,recordingDetails", video);
		updateVideosRequest.execute();




		//		YouTube.Videos.List listVideosRequest2 = youtube.videos().list("recordingDetails").setId(id);
		//		VideoListResponse listResponse2 = listVideosRequest.execute();
		//		List<Video> videoList2 = listResponse.getItems();
		//		if (videoList.isEmpty()) {
		//			return; //should not happen
		//		}
		//		Video video2 = videoList.get(0);
		//		
		//		
		//		
		//		YouTube.Videos.Update updateVideosRequest2 = youtube.videos().update("id,snippet,recordingDetails", video2);
		//		updateVideosRequest2.execute();
	}

	private void uploadThumbnail() {
		try {
			File imageFile = new File(ProgramSettings.getInstance().getThumbnailPath());
			InputStreamContent mediaContent;
			mediaContent = new InputStreamContent("image/png", new BufferedInputStream(new FileInputStream(imageFile)));
			mediaContent.setLength(imageFile.length());
			Set thumbnailSet = youtube.thumbnails().set(id, mediaContent);
			MediaHttpUploader uploader = thumbnailSet.getMediaHttpUploader();
			uploader.setDirectUploadEnabled(false);
			thumbnailSet.execute();
		} catch (IOException e) {}
	}

	/**
	 * Check if there already exists a broadcast with that name.
	 * @return the broadcast if it exists, else null
	 */
	private LiveBroadcast getExistingBroadcast() {
		UserInput ui = UserInput.getInstance();
		try {
			YouTube.LiveBroadcasts.List liveBroadcastRequest =
					youtube.liveBroadcasts().list("id,snippet");
			liveBroadcastRequest.setBroadcastStatus("upcoming"); // include all available broadcasts
			LiveBroadcastListResponse returnedListResponse = liveBroadcastRequest.execute();

			List<LiveBroadcast> returnedList = returnedListResponse.getItems();

			for (LiveBroadcast broadcast : returnedList) {
				if (broadcast.getSnippet().getTitle().equals(ui.getBroadCastTitle().trim())) {
					if (ui.isOverwriting()) {
						youtube.liveBroadcasts().delete(broadcast.getId()).execute();
					} else {
						return broadcast;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean setIdOnStartUpIfBroadcastExists(String title) {
		title = title.trim();
		boolean idSet = false;
		try {
			YouTube.LiveBroadcasts.List liveBroadcastRequest = youtube.liveBroadcasts().list("id,snippet");
			liveBroadcastRequest.setBroadcastStatus("upcoming");
			LiveBroadcastListResponse liveBroadcastListResponse = liveBroadcastRequest.execute();
			List<LiveBroadcast> broadcastList = liveBroadcastListResponse.getItems();
			for (LiveBroadcast broadcast : broadcastList) {
				if (broadcast.getSnippet().getTitle().equals(title)) {
					id = broadcast.getId();
					idSet = true;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return idSet;
	}

	public boolean transitionToComplete() {
		try {
			YouTube.LiveStreams.List listRequest = youtube.liveStreams().list("id,snippet");

			LiveStreamListResponse liveStreamListResponse = listRequest.execute();
			List<LiveStream> liveStreams = liveStreamListResponse.getItems();
			LiveStream active = null;
			for (LiveStream liveStream : liveStreams) {
				if (streamId != null && streamId.equals(liveStream.getId())) {
					active = liveStream;
					break;
				}
			}
			if (active.getStatus().getStreamStatus().equals("active")) {
				YouTube.LiveBroadcasts.Transition transition = youtube.liveBroadcasts().transition("complete", id,"id");
				transition.execute();
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

}
