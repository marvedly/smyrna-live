All files will by default be saved in "Smyrna Live Resources".

Start the program once, then extract the images.zip into "Smyrna Live Resources".

The "config"-folder, "mxlight"-folder, and "YouTube XML Folder" should be put there
by default. Possible to change those locations in "config/settings.smy".